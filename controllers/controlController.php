<?php

class control extends Controller{


    function init() {

        /* $slug = $_REQUEST['slug'];
        $type= $_REQUEST['t'];
        $data = array(
            $slug,
            ucfirst($slug)
        );
        $replace = array(
            '#nombre_controller#',
            '#nombre_clase#'
        );

        $data_esquema['data_replace']   = $data;
        $data_esquema['replace']        = $replace;

         $this->crearEsquema($slug,$data_esquema,$type);
         */
         return $this->actionSwitch();

    }

    function crearController($slug) {

        $arrReturn = array();

        $arrReturn['status']    = false;
        $arrReturn['error']     = '';

        $filename =dirname(__FILE__).'/'.$slug.'Controller.php';

        if(!file_exists($filename)) {

            $handle = fopen($filename,'w');
            $layout = file_get_contents('../app/esquema_controller.txt');
            
            $data = array(
                $slug
            );
            $replace = array(
                '#nombre_controller#'
            );
            $new_controller_text = str_replace($replace,$data,$layout);
    
            fwrite($handle,$new_controller_text);
            if(fclose($handle)){
                // $sql = "UPDATE objeto SET archivos_creados = 1 WHERE slug = '$slug' AND tipo = 'controller'";
                // return Conexion::update($sql);
                $arrReturn['status'] =true;
                $arrReturn['msj']  = 'controller creado con exito';
                return $arrReturn;
            }
        }else{
            $arrReturn['error'] = ' El controlador ya existe';
        }
        return $arrReturn;
    }
}
?>