<?php
class evento extends Controller{

    function __construct() {

        parent::__construct('evento');
    }
    function init() {

        Funciones::loadClasses('Eventos');
        global $Eventos;

        if( $id  = $this->get_request_id() ) {

            $id = filter_var($id,FILTER_SANITIZE_NUMBER_INT);
            
            $evento = $Eventos->getById($id);
            $this->evento = $evento;
            $arrReturn['evento'] = $this->evento;
            return $this->evento;

        }else{
            
            return 'hola';
        }
        

    }

    function getEventos() {
        
        Funciones::loadClasses('Eventos');
        global $Eventos;

        //obtengo la informacion que quiero mostrar
        $eventos = $Eventos->getAllWithEstado();
        return $eventos;
        
    }

    function getEstados() {

        Funciones::loadClasses('Eventos');
        global $Eventos;



        $html = '';
        $activo = '';
        $estados =  $Eventos->getEstados();
        $n_estado = 'publicado';
        
        foreach( $estados as $estado ) {

            if ( !empty( $this->evento['estado'] ) ) {
                $n_estado = $this->evento['estado'];
            }
            
            $activo = ( $estado['slug'] == $n_estado  ) ? 'activo' : '';
            $html .='<div class="item-estado '.$activo.'" data-estado="'.$estado['id'].'">'.ucfirst($estado['slug']).' <span class="item-glow '.$estado['slug'].'"></span></div>';

        }
        return $html;
    }

    function getEstadoActual() {
        
        Funciones::loadClasses('App');
        global $App;

        $id_estado = 0;

        if ( !empty( $this->evento['id_estado']) ) {

            $tt= $App->getEstadoById( $this->evento['id_estado'] )['id'];
            $id_estado = ( !empty( $tt ) && $tt != '' ) ? $tt : 0 ;

        }
        return $id_estado;

    }
    
    function getDiaEvento(){ 

        if( !empty( $this->evento['fecha_evento'] )){
            $dia = explode('-' , $this->evento['fecha_evento'] )[2] ;
            return $dia;

        }else{
            return $this->getRandomDay();
        }
    }
    function getMesEvento() {
        $nmes = Funciones::getNombresMeses();
        if( !empty( $this->evento['fecha_evento'] )){ 

            $mes = $nmes[explode('-' , $this->evento['fecha_evento'] )[1]-1] ;
            return $mes;
        }else{
            return $this->getRandomMes();
        }
    }


}

?> 