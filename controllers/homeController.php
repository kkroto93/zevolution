<?php
class home extends Controller{


    function __construct() {

        parent::__construct('home');
        

    }

    function init() {
        // sesion_start();
    }

    function test(){    
        
        Funciones::loadClasses('PathBuilders');
        global $PathBuilders;
        $arrReturn = array();


        $response = $PathBuilders->crearInit();

        return $response; 
    }

    
    
    function getDisponibles() {

        Funciones::loadClasses('Cuaderno');
        global $Cuaderno;  
        
        
        
        $cuaderno = $Cuaderno->getDisponibles('fecha_alta');
        
        return $cuaderno;

    }


    function getAppClases() {


    }


 function getAll() {

        Funciones::loadClasses('Eventos');
        global $Eventos;

        $all = $Eventos->getAll();
        
        $arrReturn = array();
        foreach ($all as $evento ) {

            $date = new DateTime($evento['fecha_creacion']);
            $dia = $date->format('d');
            $mes = $this->getMes( (int)$date->format('m') );
            $data = array();

            $data['titulo']           = $evento['titulo'];
            $data['descripcion']      = $evento['artistas'];
            $data['imagen_principal'] = $evento['imagen_principal'];
            $data['slug']             = $evento['slug'];
            $data['estado']           = $evento['estado'];            
            $data['dia']              = $dia;
            $data['mes']              = $mes;

            array_push($arrReturn,$data);
        }

            return($arrReturn);

    }
}

?> 