<?php
class logeado extends Controller{


    function __construct() {

        parent::__construct('logeado');

    }

   
    function getAll() {

        Funciones::loadClasses('Eventos');
        global $Eventos;

        $all = $Eventos->getAll();
        
        $arrReturn = array();
        foreach ($all as $evento ) {

            $date = new DateTime($evento['fecha_creacion']);
            $dia = $date->format('d');
            $mes = $this->getMes( (int)$date->format('m') );
            $data = array();

            $data['titulo']           = $evento['titulo'];
            $data['descripcion']      = $evento['artistas'];
            $data['imagen_principal'] = $evento['imagen_principal'];
            $data['slug']             = $evento['slug'];
            $data['estado']           = $evento['estado'];            
            $data['dia']              = $dia;
            $data['mes']              = $mes;

            array_push($arrReturn,$data);
        }

            return($arrReturn);

    }



}

?> 