<?php
class entrada extends Controller{

    

    function __construct() {

        parent::__construct('entrada');
        Funciones::loadClasses('Entradas');
        global $Entradas;
    }

    function init(){
        Funciones::loadClasses('Entradas');
        global $Entradas;

        if( $id  = $this->get_request_id() ) {

            $id = filter_var($id,FILTER_SANITIZE_NUMBER_INT);
            
            $entrada = $Entradas->getById($id);
            $this->entrada = $entrada;
            $arrReturn['entrada'] = $this->entrada;
            return $this->entrada;

        }else{
            
            return 'hola';
        }
    }
   function getEntradas(){

       return $this->getAll();

   }
    function getAll() {

        global $Entradas;

        $all = $Entradas->getAllWithEstado();
        // echo var_dump(  $all )  ;
        return $all;

    }
    function getEstadoActual() {

        Funciones::loadClasses('App');
        global $App;

        $slug = '';
        if ( !empty( $this->entrada['id_estado']) ) {

            $slug =  $App->getEstadoById( $this->entrada['id_estado'] )['id'];
        }
        return $slug;
        // if( !empty($this->entrada)) {

            
        // }

    }
    function getEstados() {

        global $Entradas;

        $html = '';
        $activo = '';
        $estados = $Entradas->getEstados(); 
        $n_estado = 'venta';
        
        foreach( $estados as $estado ) {           
            
            $activo = ( $estado['slug'] == $n_estado  ) ? 'activo' : '';
            $html .='<div class="item-estado '.$estado['slug'].' '.$activo.'" data-estado="'.$estado['id'].'">'.ucfirst($estado['slug']).'</div>';
            
        }
        return $html;
    }


   

    function getEventosNuevos() {
        global $Entradas;
        $eventos = $Entradas->getEventosNuevos();

        $html = '';
        $activo = '';
        $n_estado = '';
        $html .= "<select name='evento' id='input_selector' class='selector-evento' >";
        foreach ($eventos as $evento ) {
            $activo = ( $evento['titulo'] == $n_estado  ) ? 'activo' : '';

            $html .='<option class="item-entrada-evento '.$activo.'" value="'.$evento['id'].'" data-entrada-evento="'.$evento['id'].'">'.ucfirst($evento['titulo']).' <span class="item-glow '.$evento['titulo'].'"></span></option>';
            
        }
        $html .= '</select>';
        return $html;
    }

}

?> 