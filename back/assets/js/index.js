$(function(){

  $('.tabs').tabs();
  $('.sidenav').sidenav();

  var nombres_meses =  ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'];

  if( $('.flipper').length > 0 ) {

      var card = document.querySelector('.flipper');
    card.addEventListener( 'click', function() {
    card.classList.toggle('is-flipped');
  });
    
  }

  
  $('.datepicker').datepicker({
    
    
    format:'dd-m-yyyy',
    setDefaultDate: true,
    defaultDate : Date.now(),
    i18n:{
      clear:'Borrar',
      cancel:'Cancelar',
      months : ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
      monthsShort : ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
      weekdays : ['Domingo','Lunes','Martes','Miercoles','Jueves','Viernes','Sabado'],
      weekdaysShort : ['Dom','Lun','Mar','Mie','Jue','Vie','Sab'],
      weekdaysAbbrev :['D','L','M','M','J','V','S']

    },
    onClose: function(e){

      const date = $('.datepicker').val();
      var data_fecha = date.split('-');

      const dia = data_fecha[0]
      const mes = nombres_meses[parseInt(data_fecha[1]) -1 ]

      $('.e-dia').text(dia);
      $('.e-mes').text(mes);


      console.log('dia',dia)
      console.log('data_fecha',data_fecha)
      console.log('diames',mes)
    }
  });


  $('select.selector-evento').change(function(e){

    $(this).addClass('activo')
    // $('#input_estado').val()
   


  })

  var scroll_opciones = false;
  
  $('.boton-next').click(function(e) {

   
    
    $('.boton-next i').toggleClass('rotar-l-180')
    $('.contenedor-call-to-action').toggleClass('activo')
    if (!scroll_opciones) {
      
      $('.grupo-inputs').toggleClass('activo');
      window.scrollTo(0,document.body.scrollHeight);
      scroll_opciones = true;

    }else {
     
      
      window.scrollTo(0,0);

      setTimeout(function(){
        $('.grupo-inputs').toggleClass('activo');
      },200)

      scroll_opciones = false;
      

    }
    
    
  })

  var estado_open = false;

  $('.item-estado').click(function(e) {

    const estado = $(this).data('estado');

    if( !estado_open ) {

      $('.item-estado').css('display','flex');
      $('.item-estado').removeClass('activo');
      $('.contenedor-estados').addClass('activo')
      estado_open = true;

    }else {
      
        $(this).addClass('activo');
        $('.contenedor-estados').removeClass('activo')
        $('.item-estado').css('display','none');
        estado_open = false; 

    }

    $('#input_estado').val(estado);
    console.log('aca en .item-estado estado',estado)
  })

  // comprobacion para mostrar la entrada que se va a editar
  // voy a comprobar si con js puedo obtener la data del input e insertarle una clase al contenedor para que se muestre

  const actual_estado = $('#input_estado').val();
  const actual_evento = $('#id_entrada_evento').val();
  console.log('este es el estado actual antes',actual_evento)

  updateEstado(actual_estado);
  updateEntradaEvento(actual_evento);

  function updateEntradaEvento(id_evento ) {

    $('#input_selector').val(id_evento).addClass('activo')

  }
  function updateEstado(estado) {
     
    $('.item-estado').removeClass('activo');

    if ( estado == 0 ) {

      $('.item-estado').first().addClass('activo');
    }else{

      $('[data-estado="'+estado+'"').addClass('activo');
    }


  }
  console.log('este es el estado actual',actual_evento)

  // formulario entrada

  $('#formulario-entrada').submit(function(e) {


    e.preventDefault();
    // $('.boton-input').addClass('sending');

    $.ajax({
      url: '/back/_post/entrada.php',
      type: "POST",
      data: new FormData(this),
      contentType: false,
      dataType: 'json',
      cache: false,
      processData: false,
      success:function(data){

        setTimeout(function(){

          $('.boton-input').removeClass('sending').val(data.msj);

        },1000);


        if(data.status){

          setTimeout(function() { 
            
            // window.location.href = '/back/entrada/'+data.new_id+'/editar';
            
          },1500);
        }

          console.log('esta es la data sv en registro',data);
      }
  })


    console.log('formulario evento stopeado',e)

  })


  
  $('#imagen_principal').change(function(e) {
    readURL(this,'#imagen_principal_preview');
    console.log('cambio esto',e)
  })


  $('#fomulario-evento').submit(function(e) {

    e.preventDefault();
    // var data_form = $(this).serializeArray();
    // data_form = getFormData(data_form);
     $('.boton-input').addClass('sending');

    $.ajax({
      url: '/back/_post/evento.php',
      type: "POST",
      data: new FormData(this),
      contentType: false,
      dataType: 'json',
      cache: false,
      processData: false,
      success:function(data){

        setTimeout(function(){

          $('.boton-input').removeClass('sending').val(data.msj);

        },1000);
        
        if(data.status){

          setTimeout(function() { 
            
            window.location.href = '/back/evento/'+data.new_id+'/editar';
            
          },1500);
        }

          console.log('esta es la data sv en registro',data);
      }
  })


    console.log('detenido form',e);

  })

  // $('.boton').click(function(){

  //   console.log('apretando en crear')
  //   // $(this).addClass('sending');
  //   $('#fomulario-evento').submit();

  // })

  $('.input_editable').on('change',function() {
    // $('#input_estado').val()
    console.log('item que cambio',e); 
  })

  $('.action-button').click(function(e) {

    const id      = $(this).data('id');
    const post      = $(this).data('post');
    const elemento = $(this).parent().parent();
    if( confirm('Realmente queres borrar este evento?') ) {

      $.post('/back/_post/'+post+'.php',{f:'delete',id},function(data){

        if ( data.status) {
  
          console.log('estoy en ok del eevento')
          setTimeout(function(){
            $(elemento).fadeOut();
            // window.location.href = '/back/evento';
          },1500);
  
        }
  
     },'json')

    }



  })




  function readURL(input,elemento_preview) {

    if (input.files && input.files[0]) {
      var reader = new FileReader();
      
      reader.onload = function(e) {
        $(elemento_preview).attr('src', e.target.result);
      }
      
      reader.readAsDataURL(input.files[0]);
    }
  }



})