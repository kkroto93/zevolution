$(function () {
  

  $('.sidenav').sidenav();
  $('.tabs').tabs();

  startSettingColors();

  function startSettingColors() {

    const colores = $('input[type="color"]');
    $(colores).each(function(item) {


      console.log('item',colores[item]);
    })
    console.log('colores',colores)

  }

  // MECANICA PARA CUSTOM INPUT COLOR
  $('.custom-input-color').click(function(e){

    const data = $(e.target).data('name');
    const input = $('#input-'+ data );
    input.click();

    console.log('apretado para encender el picker',e);
    console.log('data',data);
    console.log('input',input);

  });

  $('input[type="color"]').change(function(e) {

    const color_value = e.target.value;
     $(e.target).prev().css({backgroundColor:color_value});

    // $('#input-'+input_name);

    console.log('el color que se eligio es',color_value);
    console.log('input_name',$(e.target));

  })

  /*SUBMIT FORM CONFIGURACION*/
  $('#form-setting-sitio').submit(function(e){
    e.preventDefault();
    var form_data = $(this).serializeArray();


    form_data = getFormData(form_data);
    $.post('_post/settings.php',{f:'update_settings',form_data :form_data},function(data){
      console.log('response sv data',data);
      if(data.status){

        toast(data.msj,'toast-success')
        setTimeout(function(){
          window.location.reload()
        },1000)
      }
    })
    console.log('data del form',form_data);
  })

  

  var swiper = new Swiper('.swiper-container', {
    pagination: {
      el: '.swiper-pagination',
      type: 'progressbar',
    },
    spaceBetween: 30,
    centeredSlides: true,
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
  });


  function toast($texto,$clase) {
    M.toast({
      html: $texto,
      classes:$clase
    });
  }

function getFormData(form) {
  var data = {};
  form.forEach(function (item) {
    var propiedad = item.name;
    var val = item.value;
    data[propiedad] = val;
  })
  return data;
}
function startLoading(){
  $('#loading').slideDown();
  $('input[type="submit"]').prop('disabled',true).css('opacity','0');
}
function stopLoading(){

  $('#loading').slideUp();
  $('input[type="submit"]').prop('disabled',false).css('opacity','1');

}
})
