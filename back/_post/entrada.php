<?php 



require_once '../../config.inc.php';
include_once '../../class/Conexion.inc.php';
include_once '../../class/Modelo.php';
include_once '../../class/Uploader.php';
include_once '../../class/Funciones.php';

$uploader = new Uploader($_FILES);
$arrReturn = array();


if( $_REQUEST['f'] == 'nuevo' ) {

    Funciones::loadClasses('Eventos');
    global $Eventos;

    $status = false;
    $msj    = 'hubo algun error';

    // echo json_encode($_REQUEST);
    // die();
    
    $id_estado    = filter_var($_REQUEST['estado'],FILTER_SANITIZE_NUMBER_INT);
    $id_evento     = filter_var($_REQUEST['evento'],FILTER_SANITIZE_STRING);
    $nombre   = filter_var($_REQUEST['nombre'],FILTER_SANITIZE_STRING);


    
    $hash_evento = $Eventos->getHashById($id_evento);

    if( !empty($hash_evento) && $hash_evento['hash_entrada'] != '' ){

        //aca llega vacio.
        $arrReturn['status'] = false;
        $arrReturn['msj'] = 'El evento ya tiene una entrada asignada';
        echo json_encode($arrReturn);
        die();
    }

    $file           = $uploader->getFile('imagen_principal');
    
    if( $file['error'] == 4 || !$file ) {

        $file   = $uploader->setFile('imagen_principal');

    }

    $upload_path    = '';

    $path   = $uploader->createImagePath($upload_path);
    
    $uploadPath = DOCUMENT_ROOT.URL_UPLOAD_ENTRADA.$path;
    
    $temp   = $uploader->getTempFile();
    $upload_image = $uploader->uploadImage($temp, $uploadPath); 
    

    if($upload_image) {                
               
        try {


            $save_path = URL_UPLOAD_ENTRADA.$path;
            $compress = Funciones::compress_image( $uploadPath,$uploadPath, 70 );
            if( $compress['status'] ) {

                Funciones::loadClasses('Entradas','Eventos');
                global $Entradas,$Eventos;
                
                $slug = Funciones::crearSlugByTitulo($nombre);
                
                $result = $Entradas->insert($id_estado, $id_evento,$slug, $nombre, $save_path);
                if($result) {

                    $id_entrada = (int)$result;
                    $update_evento = $Eventos->setEntrada($id_entrada, $id_evento);

                    $status= true;
                    $msj = "Se creo la entarda con éxito";
                    $arrReturn['new_id'] = $id_entrada;
                    $arrReturn['update_evento'] = $update_evento;
                    
                    // if($update_evento) {   

                       
                    // }
                }

            }

        }catch ( Exception $e ) {

            echo json_encode('Hubo un error :  -> '.$e->getMessage() );
            die();
        }
              
        
    }

    $arrReturn['status'] = $status;
    $arrReturn['msj']   = $msj;


    echo json_encode($arrReturn);




}

if( $_REQUEST['f'] == 'editar' ) {
    
    Funciones::loadClasses('Entradas','Eventos');
    global $Entradas,$Eventos;

    $id         = filter_var($_REQUEST['id'],FILTER_SANITIZE_NUMBER_INT);
    $id_estado    = filter_var($_REQUEST['estado'],FILTER_SANITIZE_NUMBER_INT);
    $id_evento     = filter_var($_REQUEST['evento'],FILTER_SANITIZE_STRING);
    $nombre   = filter_var($_REQUEST['nombre'],FILTER_SANITIZE_STRING);
    // echo json_encode($_REQUEST);
    // die();  

    $hash_evento = $Eventos->getHashById($id_evento);

    $file           = $uploader->getFile('imagen_principal');
    $arrReturn = array();
    $status = false;
    $msj    ='';
    if( $file['error'] == 4 || !$file ) {

        
       //aca puedo hacer update sin subir img
       
       $reset_hash = $Eventos->resetHash($id);
    //    echo var_dump($reset_hash);
    //    die();
       $update_evento = $Entradas->update($id, $id_estado, $id_evento, $nombre);

       if($update_evento ) {

            if($reset_hash) {

                $update_evento = $Eventos->setEntrada($id, $id_evento);
                if($update_evento) {

                    $status= true;
                    $msj = "Edicion exitosax";
                    
                }
            }

       }

        
       $status = $update_evento;
       $msj = 'Edicion exitosa2';

        
    }else{

    $upload_path    = '';

    $path   = $uploader->createImagePath($upload_path);
    
    $uploadPath = DOCUMENT_ROOT.URL_UPLOAD_ENTRADA.$path;
    
    $temp   = $uploader->getTempFile();
    $upload_image = $uploader->uploadImage($temp, $uploadPath); 
    

    if($upload_image) {                
               
        try {

            $save_path = URL_UPLOAD_ENTRADA.$path;
            
            $compress = Funciones::compress_image( $uploadPath, $uploadPath, 70 );

            if( $compress['status'] ) {

                
                // $slug = Funciones::crearSlugByTitulo($nombre);                
                $result = $Entradas->update($id, $id_estado, $id_evento, $nombre,$save_path);
                if($result) {

                    $reset_hash = $Eventos->resetHash($id);
                    if($reset_hash) {

                        $update_evento = $Eventos->setEntrada($id, $id_evento);
                        if($update_evento) {

                            $status= true;
                            $msj = "Edicion exitosa";

                        }
                    }

                }
                
            }
            
        }catch ( Exception $e ) {
            
            echo json_encode('Hubo un error :  -> '.$e->getMessage() );
            die();
        }
        
        
    }
    
    
}

$arrReturn['new_id'] = $id;
$arrReturn['status']    = $status;
$arrReturn['msj']       = $msj;

    echo json_encode($arrReturn);


}

if( $_REQUEST['f'] == 'delete') {
    
    Funciones::loadClasses('Entradas');
    global $Entradas;

    $arrReturn = array();
    $msj = 'Hubo algun error';
    $status = false;

    $id = filter_var($_REQUEST['id'], FILTER_SANITIZE_NUMBER_INT);

    $delete_imagen = $Entradas->deleteImagen($id);
    if($delete_imagen['status']){

        $delete_evento = $Entradas->delete($id);
    
        if($delete_evento) {
    
            $status = true;            
            $msj  = ' Se elimino el evento con exito';
            
        }
    }

    $arrReturn['status']    = $status;
    $arrReturn['msj']       = $msj;
    $arrReturn['delete_imagen']       = $msj;
    echo json_encode($arrReturn);
}






?>