<?php

require_once '../../config.inc.php';
include_once '../../class/Conexion.inc.php';
include_once '../../class/Modelo.php';
include_once '../../class/Funciones.php';

Funciones::loadClasses('Hoja');
$tabla = 'hojas';
global $Hoja;


if( $_REQUEST['f'] == 'nuevo' ) {

    global $arrReturn;
    
    
    $arrReturn['status']    = false;
    $arrReturn['f']         = $_REQUEST['f'];
    
    $nombre = filter_var($_REQUEST['nombre'], FILTER_SANITIZE_STRING );
    $disponible = ( !empty($_REQUEST['check_disponible']) ) ? 1 : 0 ;
    $result = $Hoja->insert($nombre, $disponible);        

    if($result) {

        $arrReturn['status']    = $result;
        $arrReturn['msg']       = "Se creo la hoja con exito";
        $arrReturn['id']        = $result;

    }

    echo json_encode($arrReturn);
    

}

if( $_REQUEST['f'] == 'editar' ) {

    $arrReturn['status'] = 'error';
    $arrReturn['f']         = $_REQUEST['f'];

    $nombre = filter_var($_REQUEST['nombre'], FILTER_SANITIZE_STRING );
    $id     = filter_var($_REQUEST['id'], FILTER_SANITIZE_NUMBER_INT);
    $disponible = ( isset($_REQUEST['check_disponible']) && !empty($_REQUEST['check_disponible'])   ) ? 1 : 0 ;


    $result = $Hoja->update($id, $nombre, $disponible);

    if($result) {

        $arrReturn['status']    = $result;
        $arrReturn['msg']       = "Se actualizo la hoja con exito";
        $arrReturn['id']        = $result;

    }

    echo json_encode($arrReturn);



}

if($_REQUEST['f']  == 'borrar' ) {


    $id = filter_var($_REQUEST['id'],FILTER_SANITIZE_NUMBER_INT);
 
    $delete = $Hoja->delete($id);
    if($delete ) {
 
        $arrReturn ['status']  = $delete;
        $arrReturn['msg'] = "Se elimino la tela con exito";
 
    }
    echo json_encode($arrReturn);
    die();
 
 
 }


 if($_REQUEST['f'] == 'disponible' ) {
    
    $arrReturn['status'] = false;
    $arrReturn['msg'] = "Este tipo de hoja ahora no esta disponible";
    $switch = 1;

    $id = filter_var($_REQUEST['id'],FILTER_SANITIZE_NUMBER_INT);
    $disponible  =  $_REQUEST['disponible'];
   
    
    $result = $Hoja->setDisponible( $id,$_REQUEST['disponible'], $tabla );

    if ($result) {

        $arrReturn['status']    = $result;

        if($disponible ) {

            $arrReturn['msg']   =  " Este tipo de hoja ahora esta disponible";
            $switch = 0;
        }
        
        
    }

    $arrReturn['switch'] = $switch;

    echo json_encode($arrReturn);
}


?>