<?php 



require_once '../../config.inc.php';
include_once '../../class/Conexion.inc.php';
include_once '../../class/Modelo.php';
include_once '../../class/Uploader.php';
include_once '../../class/Funciones.php';

$uploader = new Uploader($_FILES);
$arrReturn = array();


if( $_REQUEST['f'] == 'nuevo' ) {

    $status = false;
    $msj    = 'hubo algun error';

    $id_tipo    = filter_var($_REQUEST['id_tipo'],FILTER_SANITIZE_NUMBER_INT);
    $titulo     = filter_var($_REQUEST['titulo'],FILTER_SANITIZE_STRING);
    $artistas   = filter_var($_REQUEST['artistas'],FILTER_SANITIZE_STRING);
    $fecha      = new DateTime($_REQUEST['evento_fecha']);
    // $fecha      = $_REQUEST['evento_fecha'];
    $fecha      = $fecha->format('Y-m-d');
    $estado     = filter_var($_REQUEST['estado'],FILTER_SANITIZE_STRING);
    // echo json_encode($_REQUEST);
    // die();

    $file           = $uploader->getFile('imagen_principal');
    
    if( $file['error'] == 4 || !$file ) {

        $file   = $uploader->setFile('imagen_principal');

    }

    $upload_path    = '';

    $path   = $uploader->createImagePath($upload_path);
    
    $uploadPath = DOCUMENT_ROOT.URL_UPLOAD_ENTRADA_EVENTO.$path;
    
    $temp   = $uploader->getTempFile();
    $upload_image = $uploader->uploadImage($temp, $uploadPath); 
    

    if($upload_image) {                
               
        try {


            $save_path = URL_UPLOAD_ENTRADA_EVENTO.$path;
            $compress = Funciones::compress_image( $uploadPath,$uploadPath, 70 );
            if( $compress['status'] ) {

                Funciones::loadClasses('Eventos');
                global $Eventos;
                
                $slug = Funciones::crearSlugByTitulo($titulo);
                
                $result = $Eventos->insert($titulo, $slug, $save_path, $artistas, $estado, $fecha);
                if($result) {

                    $status= true;
                    $msj = "Se creo el evento con éxito";
                    $arrReturn['new_id'] = $result;
                }

            }

        }catch ( Exception $e ) {

            echo json_encode('Hubo un error :  -> '.$e->getMessage() );
            die();
        }
              
        
    }

    $arrReturn['status'] = $status;
    $arrReturn['msj']   = $msj;


    echo json_encode($arrReturn);




}

if( $_REQUEST['f'] == 'editar' ) {
    
    $id         = filter_var($_REQUEST['id'],FILTER_SANITIZE_NUMBER_INT);
    $titulo     = filter_var($_REQUEST['titulo'],FILTER_SANITIZE_STRING);
    $artistas   = filter_var($_REQUEST['artistas'],FILTER_SANITIZE_STRING);
    $fecha      = new DateTime($_REQUEST['evento_fecha']);
    // $fecha      = $_REQUEST['evento_fecha'];
    $fecha      = $fecha->format('Y-m-d');
    $estado     = filter_var($_REQUEST['estado'],FILTER_SANITIZE_STRING);


    // echo json_encode($_REQUEST);
    // die();  
    $file           = $uploader->getFile('imagen_principal');
    $arrReturn = array();
    $status = false;
    $msj    ='';
    if( $file['error'] == 4 || !$file ) {

        Funciones::loadClasses('Eventos');
        global $Eventos;
       //aca puedo hacer update sin subir img
       $update_evento = $Eventos->Update($id, $titulo, $artistas, $estado, $fecha);
        
       $status = $update_evento;
       $msj = 'Edicion exitosa';

        
    }else{

    $upload_path    = '';

    $path   = $uploader->createImagePath($upload_path);
    
    $uploadPath = DOCUMENT_ROOT.URL_UPLOAD_ENTRADA_EVENTO.$path;
    
    $temp   = $uploader->getTempFile();
    $upload_image = $uploader->uploadImage($temp, $uploadPath); 
    

    if($upload_image) {                
               
        try {

            $save_path = URL_UPLOAD_ENTRADA_EVENTO.$path;
            
            $compress = Funciones::compress_image( $uploadPath, $uploadPath, 70 );

            if( $compress['status'] ) {

                Funciones::loadClasses('Eventos');
                global $Eventos;
                
                $slug = Funciones::crearSlugByTitulo($titulo);                
                $result = $Eventos->update($id, $titulo, $artistas, $estado, $fecha, $save_path);

                if($result) {

                    $status= true;
                    $msj = "Edicion exitosa";

                }
                
            }
            
        }catch ( Exception $e ) {
            
            echo json_encode('Hubo un error :  -> '.$e->getMessage() );
            die();
        }
        
        
    }
    
    
}

$arrReturn['new_id'] = $id;
$arrReturn['status']    = $status;
$arrReturn['msj']       = $msj;

    echo json_encode($arrReturn);


}

if( $_REQUEST['f'] == 'delete') {
    
    Funciones::loadClasses('Eventos');
    global $Eventos;

    $arrReturn = array();
    $msj = 'Hubo algun error';
    $status = false;

    $id = filter_var($_REQUEST['id'], FILTER_SANITIZE_NUMBER_INT);

    $delete_imagen = $Eventos->deleteImagen($id);
    if($delete_imagen['status']){

        $delete_evento = $Eventos->delete($id);
    
        if($delete_evento) {
    
            $status = true;            
            $msj  = ' Se elimino el evento con exito';
            
        }
    }

    $arrReturn['status']    = $status;
    $arrReturn['msj']       = $msj;
    echo json_encode($arrReturn);
}






?>