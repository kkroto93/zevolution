<?php
require_once '../../config.inc.php';
include_once '../../class/Conexion.inc.php';
include_once '../../class/Modelo.php';
include_once '../../class/Funciones.php';
include_once '../../class/Uploader.php';



ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
Funciones::loadClasses('Cuaderno');

global $Cuaderno;

$uploader = new Uploader($_FILES);
$arrReturn = array();
$tabla = 'cuadernos';


if( $_REQUEST['f'] == 'nuevo' ) {

    global $arrReturn;
    
    
    $arrReturn['status']    = false;
    $arrReturn['f']         = $_REQUEST['f'];

    $nombre = filter_var($_REQUEST['nombre'], FILTER_SANITIZE_STRING );
    $precio = filter_var($_REQUEST['precio'], FILTER_SANITIZE_NUMBER_INT);    
    $disponible = ( !empty($_REQUEST['check_disponible']) ) ? 1 : 0 ;
    // echo json_encode($disponible);
    // die();
    //UPLODAR IMAGENES
    
    
    $upload_path    = URL_UPLOAD_CUADERNOS;
    $path           = '';
    $result         = false;
    $upload_image   = false;

    $file           = $uploader->getFile('imagen_principal');
    
    if( $file['error'] == 4 || !$file ) {

        $file   = $uploader->setFile('imagen_desktop');

    }

    if($file) {

        $status = $uploader->isNewImageImage( $upload_path );
        

        if($status['status'] ) {

            $path   = $uploader->createImagePath($upload_path);
            
            
            $f_path = URL_SERVER_UPLOAD.$path;
            
            $temp   = $uploader->getTempFile();
            $upload_image = $uploader->uploadImage($temp, $f_path);

            if($upload_image) {

                
               
                try {
                    $compress = Funciones::compress_image( '../../'.$path,$f_path, 50 );
                    if( $compress['status']) {
                        
                        $result = $Cuaderno->insert($nombre,$path, $disponible,$precio);

                    }

                }catch ( Exception $e ) {

                    echo json_encode('Hubo un error :  -> '.$e->getMessage() );
                    die();
                }
                      
                
            }
            
            
        }
        else{
            
            $result = $Cuaderno->insert($nombre,$path, $disponible,$precio);

        }

        if($result) {
        
            $arrReturn['status']    = $result;
            $arrReturn['msg']       = 'Se creo el cuaderno con exito!';
            $arrReturn['id']        = $result;
            
            

        }
        else{

            $arrReturn['msg']       = 'Hubo un error al crear el cuaderno';

        }

    }

    
    
    
    echo json_encode($arrReturn,true);

}

if($_REQUEST['f'] == 'disponible' ) {
    
    $arrReturn['status'] = false;
    $arrReturn['msg'] = "Este cuaderno ahora no esta disponible";
    $switch = 1;

    $id = filter_var($_REQUEST['id'],FILTER_SANITIZE_NUMBER_INT);
    $disponible  =  $_REQUEST['disponible'];
   
    
    $result = $Cuaderno->setDisponible( $id,$_REQUEST['disponible'], $tabla );

    if ($result) {

        $arrReturn['status']    = $result;

        if($disponible ) {

            $arrReturn['msg']   =  " Este cuaderno ahora esta disponible";
            $switch = 0;
        }
        
        
    }

    $arrReturn['switch'] = $switch;

    echo json_encode($arrReturn);
}

if($_REQUEST['f']  == 'borrar' ) {


   $id = filter_var($_REQUEST['id'],FILTER_SANITIZE_NUMBER_INT);

   $delete = $Cuaderno->delete($id);
   if($delete ) {

       $arrReturn ['status']  = $delete;
       $arrReturn['msg'] = "Se elimino el cuaderno con exito";

   }
   echo json_encode($arrReturn);
   die();


}


if( $_REQUEST['f'] == 'editar' ) {

    header('Content-Type: application/json');
    $arrReturn['status'] = 'error';
    $arrReturn['f']         = $_REQUEST['f'];
    
    


    $nombre = filter_var($_REQUEST['nombre'], FILTER_SANITIZE_STRING );
    $id     = filter_var($_REQUEST['id'], FILTER_SANITIZE_NUMBER_INT);
    $precio     = filter_var($_REQUEST['precio'], FILTER_SANITIZE_NUMBER_INT);
    $disponible = ( isset($_REQUEST['check_disponible']) && !empty($_REQUEST['check_disponible'])   ) ? 1 : 0 ;

    $upload_path = URL_UPLOAD_CUADERNOS;


    $file   = $uploader->getFile('imagen_principal');

    if( $file['error'] == 4 || !$file ) {

        $file   = $uploader->setFile('imagen_desktop');

    }
    // echo json_encode( $file);
    // die();
    

    $status = $uploader->isNewImageImage($upload_path, false);

    if($status['status'] ) {
        //ACA HAY IMAGEN
        $path   = $uploader->createImagePath($upload_path);
        $f_path = URL_SERVER_UPLOAD.$path;
        $temp   = $uploader->getTempFile();

        $upload_image = $uploader->uploadImage($temp, $f_path);

        if($upload_image){
            
            $update = $Cuaderno->update($id, $nombre, $disponible,$precio,$path);
            if($update)  {

                $arrReturn['msg'] = "Se updateo cambiando la imagen";
                $arrReturn['status'] = $update;

            }
            echo json_encode($arrReturn,true);
        }

    }else{
        //ACA NO HAY IMAGEN
        //solo updateo los otros campos
        $update     = $Cuaderno->update($id, $nombre,$disponible,$precio);
        if($update) {

            $arrReturn['msg'] = "Se updateo sin cambiar la imagen";
            $arrReturn['status'] = $update;
        }
        echo json_encode($arrReturn,true);
        
    }  

}
// function uploadImage( $temp, $path) { 
//     return 
// }








?>