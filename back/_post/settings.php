<?php 


session_start();
require_once '../../config.inc.php';

header('Content-Type: application/json');

if($_REQUEST['f'] == 'update_settings' ) {

    $arrReturn = array();
    $status = false;
    $msj = 'Hubo algun error al updatear settings';

    // echo json_encode($_REQUEST);
    // die();

    if ( Sesion::checkLogin() ) {

        Funciones::loadClasses('App');
        global $App;
        
        if( !empty( $_REQUEST['form_data']) ) {
            
            $update = array();
            $status = false;
            $msj = 'Error al actualizar settings';

            foreach($_REQUEST['form_data'] as $os_field => $valor ) {

                if(substr($os_field,0,3) == 'os_') {

                    // BUSCO EL sitio_opcion_id
                    $arr = explode('_',$os_field);
                    $id_propiedad = (int) end($arr);
        
                   if ( $App->updateByIdPropiedad($id_propiedad,$valor) ){
                       $status = true;
                   }
        
                }

            }

            if ( $status ) {

                $msj = 'Setting Actualizados';

            }
            $arrReturn['status']    = $status;
            $arrReturn['msj']       = $msj;



            echo json_encode($arrReturn);
            die();
        }

    }
    else{

        // header("Location: ".SITIO_URL_FRONTEND.'back');
        $status = false;
        $msj = 'tenes que logearte';

    }   

    $arrReturn['status'] = $status;
    $arrReturn['msj']  = $msj;

    echo json_encode( $arrReturn);

}






?>