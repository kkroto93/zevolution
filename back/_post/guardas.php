<?php
require_once '../../config.inc.php';
include_once '../../class/Conexion.inc.php';
include_once '../../class/Modelo.php';
include_once '../../class/Funciones.php';
include_once '../../class/Uploader.php';

header('Content-Type: application/json');
Funciones::loadClasses('Guarda');

global $Guarda;
$uploader = new Uploader($_FILES);
$arrReturn = array();
$tabla = 'guardas';

if( $_REQUEST['f'] == 'nuevo' ) {

    global $arrReturn;
    
    
    $arrReturn['status']    = false;
    $arrReturn['f']         = $_REQUEST['f'];

    $nombre = filter_var($_REQUEST['nombre'], FILTER_SANITIZE_STRING );
    $disponible = ( !empty($_REQUEST['check_disponible']) ) ? 1 : 0 ;
    // echo json_encode($disponible);
    // die();
    //UPLODAR IMAGENES
    
    $upload_path    = URL_UPLOAD_GUARDAS;
    $path           = '';
    $result;
    $upload_image   = false;

    $file = $uploader->getFile('imagen_principal');

    if( $file['error'] == 4 || !$file ) {

        $file   = $uploader->setFile('imagen_desktop');

    }

    if($file) {

        $status = $uploader->isNewImageImage( $upload_path);

        if($status['status'] ) {

            $path   = $uploader->createImagePath($upload_path);
            $f_path = URL_SERVER_UPLOAD.$path;
            // echo json_encode($path);
            // die();
            $temp   = $uploader->getTempFile();
            $upload_image = $uploader->uploadImage($temp, $f_path);

            if($upload_image) {

                $result = $Guarda->insert($nombre,$path, $disponible);        
                
            }
            
            
        }
        else{
            
            $result = $Guarda->insert($nombre,$path, $disponible);

        }

        if($result) {
        
            $arrReturn['status']    = $result;
            $arrReturn['msg']       = 'Se creo la guarda con exito!';
            $arrReturn['id']        = $result;
            
            

        }
        else{

            $arrReturn['msg']       = 'Hubo un error al crear la guarda';

        }

    }


    

    
    
    
    echo json_encode($arrReturn,true);

}



if($_REQUEST['f'] == 'disponible' ) {
    
    $arrReturn['status'] = false;
    $arrReturn['msg'] = "Esta tela ahora no esta disponible";
    $switch = 1;

    $id = filter_var($_REQUEST['id'],FILTER_SANITIZE_NUMBER_INT);
    $disponible  =  $_REQUEST['disponible'];
   
    
    $result = $Guarda->setDisponible( $id,$_REQUEST['disponible'],$tabla );

    if ($result) {

        $arrReturn['status']    = $result;

        if($disponible ) {

            $arrReturn['msg']   =  " Esta tela ahora esta disponible";
            $switch = 0;
        }
        
        
    }

    $arrReturn['switch'] = $switch;

    echo json_encode($arrReturn);
}

if($_REQUEST['f']  == 'borrar' ) {


    $id = filter_var($_REQUEST['id'],FILTER_SANITIZE_NUMBER_INT);
 
    $delete = $Guarda->delete($id);
    if($delete ) {
 
        $arrReturn ['status']  = $delete;
        $arrReturn['msg'] = "Se elimino la guarda con exito";
 
    }
    echo json_encode($arrReturn);
    die();
 
 
 }


 if( $_REQUEST['f'] == 'editar' ) {

    header('Content-Type: application/json');
    $arrReturn['status'] = 'error';
    $arrReturn['f']         = $_REQUEST['f'];
    
    


    $nombre = filter_var($_REQUEST['nombre'], FILTER_SANITIZE_STRING );
    $id     = filter_var($_REQUEST['id'], FILTER_SANITIZE_NUMBER_INT);
    $disponible = ( isset($_REQUEST['check_disponible']) && !empty($_REQUEST['check_disponible'])   ) ? 1 : 0 ;

    $upload_path = URL_UPLOAD_GUARDAS;
    
    $file   = $uploader->getFile('imagen_principal');
    if( $file['error'] == 4 || !$file ) {

        $file   = $uploader->setFile('imagen_desktop');

    }

    $status = $uploader->isNewImageImage($upload_path, false);

    if($status['status'] ) {
        //ACA HAY IMAGEN
        $path   = $uploader->createImagePath($upload_path);
        $f_path = URL_SERVER_UPLOAD.$path;
        $temp   = $uploader->getTempFile();

        $upload_image = $uploader->uploadImage($temp, $f_path);
        if($upload_image){
            
            $update = $Guarda->update($id, $nombre, $disponible,$path);
            if($update)  {

                $arrReturn['msg'] = "Se updateo cambiando la imagen";
                $arrReturn['status'] = $update;

            }
            echo json_encode($arrReturn,true);
        }

    }else{
        //ACA NO HAY IMAGEN
        //solo updateo los otros campos
        $update     = $Guarda->update($id, $nombre,$disponible);
        if($update) {

            $arrReturn['msg'] = "Se updateo sin cambiar la imagen";
            $arrReturn['status'] = $update;
        }
        echo json_encode($arrReturn,true);
        
    }  

}


?>