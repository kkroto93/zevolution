<?php
require_once '../../config.inc.php';
include_once '../../class/Conexion.inc.php';
include_once '../../class/Modelo.php';
include_once '../../class/Funciones.php';
include_once '../../class/Uploader.php';

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
Funciones::loadClasses('Tela');

global $Tela;

$uploader = new Uploader($_FILES);
$arrReturn = array();
$tabla = 'telas';


if( $_REQUEST['f'] == 'nuevo' ) {

    global $arrReturn;
    
    
    $arrReturn['status']    = false;
    $arrReturn['f']         = $_REQUEST['f'];

    $nombre = filter_var($_REQUEST['nombre'], FILTER_SANITIZE_STRING );
    $disponible = ( !empty($_REQUEST['check_disponible']) ) ? 1 : 0 ;
    // echo json_encode($disponible);
    // die();
    //UPLODAR IMAGENES
    
    

    $upload_path    = URL_UPLOAD_TELAS;
    $path           = '';
    $result         = false;
    $upload_image   = false;

    $file           = $uploader->getFile('imagen_principal');
    if( $file['error'] == 4 || !$file ) {

        $file   = $uploader->setFile('imagen_desktop');

    }

    if($file) {

        $status = $uploader->isNewImageImage( $upload_path);

        if($status['status'] ) {

            $path   = $uploader->createImagePath($upload_path);
            $f_path = URL_SERVER_UPLOAD.$path;
            $temp   = $uploader->getTempFile();
            $upload_image = $uploader->uploadImage($temp, $f_path);

            if($upload_image) {

                $result = $Tela->insert($nombre,$path, $disponible);        
                
            }
            
            
        }
        else{
            
            $result = $Tela->insert($nombre,$path, $disponible);

        }

        if($result) {
        
            $arrReturn['status']    = $result;
            $arrReturn['msg']       = 'Se creo la tela con exito!';
            $arrReturn['id']        = $result;
            
            

        }
        else{

            $arrReturn['msg']       = 'Hubo un error al crear la tela';

        }

    }

    
    
    
    echo json_encode($arrReturn,true);

}

if($_REQUEST['f'] == 'disponible' ) {
    
    $arrReturn['status'] = false;
    $arrReturn['msg'] = "Esta tela ahora no esta disponible";
    $switch = 1;

    $id = filter_var($_REQUEST['id'],FILTER_SANITIZE_NUMBER_INT);
    $disponible  =  $_REQUEST['disponible'];
   
    
    $result = $Tela->setDisponible( $id,$_REQUEST['disponible'], $tabla );

    if ($result) {

        $arrReturn['status']    = $result;

        if($disponible ) {

            $arrReturn['msg']   =  " Esta tela ahora esta disponible";
            $switch = 0;
        }
        
        
    }

    $arrReturn['switch'] = $switch;

    echo json_encode($arrReturn);
}

if($_REQUEST['f']  == 'borrar' ) {


   $id = filter_var($_REQUEST['id'],FILTER_SANITIZE_NUMBER_INT);

   $delete = $Tela->delete($id);
   if($delete ) {

       $arrReturn ['status']  = $delete;
       $arrReturn['msg'] = "Se elimino la tela con exito";

   }
   echo json_encode($arrReturn);
   die();


}


if( $_REQUEST['f'] == 'editar' ) {

    header('Content-Type: application/json');
    $arrReturn['status'] = 'error';
    $arrReturn['f']         = $_REQUEST['f'];
    
    


    $nombre = filter_var($_REQUEST['nombre'], FILTER_SANITIZE_STRING );
    $id     = filter_var($_REQUEST['id'], FILTER_SANITIZE_NUMBER_INT);
    $disponible = ( isset($_REQUEST['check_disponible']) && !empty($_REQUEST['check_disponible'])   ) ? 1 : 0 ;

    $upload_path = URL_UPLOAD_TELAS;
    $file   = $uploader->getFile('imagen_principal');
    if( $file['error'] == 4 || !$file ) {

        $file   = $uploader->setFile('imagen_desktop');

    }

    $status = $uploader->isNewImageImage($upload_path, false);

    if($status['status'] ) {
        //ACA HAY IMAGEN
        $path   = $uploader->createImagePath($upload_path);
        $f_path = URL_SERVER_UPLOAD.$path;
        $temp   = $uploader->getTempFile();

        $upload_image = $uploader->uploadImage($temp, $f_path);
        if($upload_image){
            
            $update = $Tela->update($id, $nombre, $disponible,$path);
            if($update)  {

                $arrReturn['msg'] = "Se updateo cambiando la imagen";
                $arrReturn['status'] = $update;

            }
            echo json_encode($arrReturn,true);
        }

    }else{
        //ACA NO HAY IMAGEN
        //solo updateo los otros campos
        $update     = $Tela->update($id, $nombre,$disponible);
        if($update) {

            $arrReturn['msg'] = "Se updateo sin cambiar la imagen";
            $arrReturn['status'] = $update;
        }
        echo json_encode($arrReturn,true);
        
    }  

}
// function uploadImage( $temp, $path) { 
//     return 
// }








?>