<?php
session_start();

require_once '../../config.inc.php';
require_once '../../class/Sesion.php';
require_once '../../class/Conexion.inc.php';



if ( !empty($_POST) ) {

    
    if ( !empty($_POST['email']) && !empty($_POST['password']) ) {
        
        Conexion::obtener_conexion();

        $email      = $_POST['email'];
        $password   = $_POST['password'];
        
        $sql = "SELECT * FROM usuario WHERE email = '$email' AND password = '$password' AND privilegio = 'admin';";
        $usuario = Conexion::select($sql,'row');

        if( !empty($usuario) && count( $usuario ) > 0 ) {

            $login = Sesion::start($email);
            if ( $login['status']){

                header('Location: '.SITIO_URL_FRONTEND.'back');
            }
           


        }
    }   

}

if( !empty($_SESSION['login']) ) {

    header('Location: '.SITIO_URL_FRONTEND.'back');
}


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <title>Acceso a Administracion</title>
    <style>
        body{
            background-color:#673AB7;
        }
        h1{
            color:white;
            font-size:20px;
            text-align:center;
            font-family:'Roboto';
        }
        .btn.login {
            background-color:#9c27b0;
            width:100%;
        }
        input {
             color:white;
        }
    </style>
</head>
<body>

<div class="container">
    <h1>Administracion</h1>
    <form action="" method="post" style="max-width:550px;margin:0 auto;">
        <input type="text" name="email" placeholder="Ingresá tu e-mail">
        <input type="password" name="password" required placeholder="Ingresá tu contraseña">
        <input type="submit" value="Ingresar" class="btn login">
    </form> 
</div>
    
</body>
</html>