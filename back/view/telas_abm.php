<?php
$data['class'] = 'telas_abm';
$data['btn_text'] ='Crear';
$id = ( isset($_REQUEST['id']) and !empty($_REQUEST['id']) ) ? $_REQUEST['id'] : 0;
Funciones::loadClasses('Tela');
global $Tela;

if($id){


    $tela = $Tela->get($id);  
    $data['btn_text'] ='Editar';  
    // echo var_dump($tela);

}
$next = $Tela::getNext('telas')['next'] ;
// echo var_dump($next);
// die();
?>


<form id="form_<?=$data['class']?>" class="form_<?=$data['class']?>">

    
    <input type="hidden" name="f" value="<?= ( !empty($tela['id']) ) ? 'editar' : 'nuevo';?>">
    <input type="hidden" name="id" value="<?= ( !empty($tela['id']) ) ? $tela['id'] : 0;?>">
    <input type="hidden" id="input_disponible" name="disponible" value="<?= ( !empty($tela['disponible']) ) ? $tela['disponible'] : 0;?>" >
    
    <div class="contendor-imagen">
    
    <label for="imagen_principal" class="imagen_principal_label">
            
            <label for="subir_desktop">
                <input type="file" id="subir_desktop" accept="image/*" name="imagen_desktop" style="display:none;">
                <i class="fas fa-folder" ></i>
            </label>
            <i class="fas fa-camera"></i>
            <i class="fas fa-times" id="borrar_imagen" style="display:<?= ( !empty($cuaderno['id']) ) ? 'flex' : 'none';?>;"></i>
            
            <input id="imagen_principal" type="file" accept="image/*" value="" name="imagen_principal" capture>
        </label>
        <img data-new="0" src="<?=( !empty($tela['imagen_principal']) ) ? SITIO_URL_FRONTEND.$tela['imagen_principal'] : '';?>" id="profile-img-tag"  />
        
        <label for="check_disp_<?= ( !empty($tela['id']) ) ? $tela['id'] : $next;?>" >
        <input type="checkbox"  id="check_disp_<?= ( !empty($tela['id']) ) ? $tela['id'] : $next;?>" <?= ( !empty($tela['disponible'] )  ) ? 'checked ' : '';?> style="display:none;" name="check_disponible" value="si">
        <div class="contenedor-disponible <?= ( !empty($tela['disponible'] ) ) ? 'negative' : '';?>" style="display:<?= ( !empty($tela['id']) ) ? 'flex;' : '';?>"  >
            
        <i class="fas fa-star <?= ( !empty($tela['disponible'] ) ) ? 'disponible_activo' : '';?>" id="disponible" ></i>
        </div>
        </label>


    </div>


    <label for="">
    Nombre de la tela
        <input type="text" name="nombre" placeholder="Nombre de la tela"  value="<?= ( !empty($tela['nombre']) ) ? $tela['nombre'] : '';?>">
    </label>
   
    
    <!-- <label for="">
        Galeria
        <input type="file" accept="image/*" name="galeria" capture multiple>
    </label> -->
    <div class="send">
        <div class="img"> 
            <img src="../images/loading.gif" alt="">
        </div>
        <input type="submit" value="<?=$data['btn_text']?>" class="btn">
    </div>

</form>