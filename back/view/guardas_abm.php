<?php
$data['class'] = 'guardas_abm';
$data['btn_text'] ='Crear';

$id = ( isset($_REQUEST['id']) and !empty($_REQUEST['id']) ) ? $_REQUEST['id'] : 0;
Funciones::loadClasses('Guarda');

global $Guarda;
if($id){


    $guarda = $Guarda->get($id);   
    $data['btn_text'] = 'Editar'; 
    
}
$next = $Guarda::getNext('guardas')['next'] ;
?>


<form id="form_<?=$data['class']?>" class="form_<?=$data['class']?>">

    
    <input type="hidden" name="f" value="<?= ( !empty($guarda['id']) ) ? 'editar' : 'nuevo';?>">
    <input type="hidden" name="id" value="<?= ( !empty($guarda['id']) ) ? $guarda['id'] : 0;?>">
    <input type="hidden" id="input_disponible" name="disponible" value="<?= ( !empty($guarda['disponible']) ) ? $guarda['disponible'] : 0;?>" >
    
    <div class="contendor-imagen">
    
        <label for="imagen_principal" class="imagen_principal_label">
            
            <label for="subir_desktop">
                <input type="file" id="subir_desktop" accept="image/*" name="imagen_desktop" style="display:none;">
                <i class="fas fa-folder" ></i>
            </label>
            <i class="fas fa-camera"></i>
            <i class="fas fa-times" id="borrar_imagen" style="display:<?= ( !empty($cuaderno['id']) ) ? 'flex' : 'none';?>;"></i>
            
            <input id="imagen_principal" type="file" accept="image/*" value="" name="imagen_principal" capture>
        </label>
        
        <img data-new="0" src="<?=( !empty($guarda['imagen_principal']) ) ? SITIO_URL_FRONTEND.$guarda['imagen_principal'] : '';?>" id="profile-img-tag"  />
        
        <label for="check_disp_<?= ( !empty($guarda['id']) ) ? $guarda['id'] : $next;?>" >
        <input type="checkbox"  id="check_disp_<?= ( !empty($guarda['id']) ) ? $guarda['id'] : $next;?>" <?= ( !empty($guarda['disponible'] )  ) ? 'checked ' : '';?> style="display:none;" name="check_disponible" value="si">
        <div class="contenedor-disponible <?= ( !empty($guarda['disponible'] ) ) ? 'negative' : '';?>" style="display:<?= ( !empty($guarda['id']) ) ? 'flex;' : '';?>"  >
            
        <i class="fas fa-star <?= ( !empty($guarda['disponible'] ) ) ? 'disponible_activo' : '';?>" id="disponible" ></i>
        </div>
        </label>


    </div>


    <label for="">
    Nombre de la guarda
        <input type="text" name="nombre" placeholder="Nombre de la guarda" required value="<?= ( !empty($guarda['nombre']) ) ? $guarda['nombre'] : '';?>">
    </label>
   
    
    <!-- <label for="">
        Galeria
        <input type="file" accept="image/*" name="galeria" capture multiple>
    </label> -->
    <input type="submit" value="<?=$data['btn_text']?>" class="btn">

</form>