<?php
$data['class'] = 'hojas_abm';
$id = ( isset($_REQUEST['id']) and !empty($_REQUEST['id']) ) ? $_REQUEST['id'] : 0;

if($id){

    Funciones::loadClasses('Hoja');

    global $Hoja;
    $hoja = $Hoja->get($id);    
    
}

?>


<form id="form_<?=$data['class']?>" class="form_<?=$data['class']?>">

    
    <input type="hidden" name="f" value="<?= ( !empty($hoja['id']) ) ? 'editar' : 'nuevo';?>">
    <input type="hidden" name="id" value="<?= ( !empty($hoja['id']) ) ? $hoja['id'] : 0;?>">
    <input type="hidden" id="input_disponible" name="disponible" value="<?= ( !empty($hoja['disponible']) ) ? $hoja['disponible'] : 0;?>" >
    
    <!-- Switch -->
  <div class="switch">
    <label>
      Disponible
      <input name="check_disponible" <?= ( !empty($hoja['disponible']) ) ? 'checked' : '';?> type="checkbox">
      <span class="lever"></span>
      
    </label>
  </div>


    <label for="">
    Tipo de Hoja
        <input type="text" name="nombre" placeholder="Escribi el tipo de hoja" required value="<?= ( !empty($hoja['nombre']) ) ? $hoja['nombre'] : '';?>">
    </label>
   
    
    <input type="submit" value="Crear" class="btn">

</form>