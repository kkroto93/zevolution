<?php

$nombre = 'hoja';
$nombre_p = $nombre.'s';

$data['titulo'] = "Esto es ".ucwords($nombre_p);
$data['class'] = $nombre_p;

Funciones::loadClasses('Hoja');
global $Hoja;

$hojas = $Hoja::getAll($nombre_p,'fecha_alta');


?>


<h1 class="titulo">Hojas</h1>
<main class="listado-<?=$data['class']?>">

<?= Funciones::generateItems($hojas,$data['class'],"Hoja::crearHoja")?>

</main>
<div class="fixed-action-btn">
  <a class="btn-floating btn-large red" href="hojas_abm">
    <i class="large material-icons"><i class="fas fa-plus"></i></i>
  </a>
</div>