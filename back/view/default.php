
<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Administracion</title>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <link rel="stylesheet" 
    href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" 
    integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" 
    crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
  
    <link rel="stylesheet" href="../css/index.css">



</head>
<body>
<div class="header">
        <i class="fas fa-bars icono sidenav-trigger" data-target="slide-out"></i>
        <div class="titulo">
            <a href="/">Ferchala</a>
        </div>
</div>

<ul id="slide-out" class="sidenav">
  <li>
        <div class="user-view">
            <div class="background">
                <img src="images/perfil2.jpg">
            </div>
        </div>
  </li>    
  <li><a href="./cuadernos">Cuadernos</a>  </li>
  <li><a href="./telas">Telas</a>  </li>
  <li><div class="divider"></div></li>
  <li><a href="./hojas"> Tipos de hojas</a> </li>
  <li><div class="divider"></div></li>
  <li><a href="./guardas"> Papel de guarda</a></li>
</ul>

<div class="container">
  <?=$html?>
</div>
    

  <script
  src="http://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
  

  <script src="./js/index.js"></script>
</body>
</html>