<?php

require '../../config.inc.php';

/* echo json_encode($_REQUEST); */
$arrReturn = array();


if ($_REQUEST['f'] == 'builder' ) {

    Funciones::loadClasses('App');
    global $App;

    $arrReturn['status']    = false;
    $arrReturn['msj']       = 'Hubo algun error al crear la app';

    //crear nueva app.
    $app_name   = filter_var($_REQUEST['app_name'],FILTER_SANITIZE_STRING);
    $app_url    = filter_var($_REQUEST['app_url'],FILTER_SANITIZE_STRING);
    $dominio    = filter_var($_REQUEST['dominio'],FILTER_SANITIZE_STRING);

    if( empty( $App->getByDominio($dominio) ) ) {

        $new_app = $App->createApp($app_name, $app_url, $dominio);
        $arrReturn['new_app'] =$new_app;

        if($new_app){
            
            $new_app = (int) $new_app;
            $opcionesValor = $App->setOpcionesValor( $new_app );
            $arrReturn['status'] = true;
            $arrReturn['setOpcionesValor'] = $opcionesValor;

            $arrReturn['msj'] ='App creada con éxito';

        }

    }else{
        $arrReturn['status'] = false;
        $arrReturn['msj'] = 'El dominio ya existe';
        $arrReturn['request'] = $_REQUEST;

    }
    

    echo json_encode($arrReturn); 

}


?>