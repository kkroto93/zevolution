 
<?php
error_reporting(E_ALL );
session_start();
require      '../vendor/autoload.php';
require_once '../config.inc.php';
require_once '../class/Conexion.inc.php';

header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1.
header("Pragma: no-cache"); // HTTP 1.0.
header("Expires: 0"); // Proxies.

// echo var_dump($_REQUEST); 
//  die();  

$p              = 'home';
$template_name  = 'home';
$data['class']  = 'home';
$data           =  array();

$app_name = 'Duing Builder ' ;

if( !empty($_REQUEST['p']) && !empty($_REQUEST['t']) && !empty($_REQUEST['a'])) {

  $p              =  $_REQUEST['p'];
  $template_name  =  $p;
  $data['class']  =  $p;
 


}

/* echo var_dump($_REQUEST);
die(); */



Funciones::loadController($p);
/* echo var_dump($controller);
die(); */
$op   = array(
  'extension'       =>  '.html'
);
$options =  array(
  
  'loader'          => new Mustache_Loader_FilesystemLoader(dirname(__FILE__).'/view/templates',$op ),
  'partials_loader' => new Mustache_Loader_FilesystemLoader(dirname(__FILE__).'/view/templates/partials',$op)
  
  
);


$m = new Mustache_Engine( $options );

$template = $m->loadTemplate($template_name);

$data['@'] = $controller;
$data['extra'] = array(

  'checked' => function($disponible, Mustache_LambdaHelper $helper) {

    $disponible = $helper->render($disponible);
    $result = ( $disponible == 1) ? 'disponible_activo' : '';
    return $result;
  },
  'app_name' => $app_name
  
);

$html = $template->render($data);

$isBack = false;


?>


<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <link rel="apple-touch-icon" sizes="180x180" href="./assets/images/icons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="72x72" href="./assets/images/icons/icon-72x72.png">
    <link rel="icon" type="image/png" sizes="96x96" href="./assets/images/icons/icon-96x96.png">

    <link rel="mask-icon" href="./assets/images/icons/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="manifest" type="application/json" href="../manifesto.json">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,700|Roboto|Open+Sans:400,600,700" rel="stylesheet">

    
    

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <link rel="stylesheet" 
    href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" 
    integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" 
    crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <link rel="stylesheet" href="<?=SITIO_URL_FRONTEND?>builder/assets/css/swiper.min.css">
    <link rel="stylesheet" href="<?=SITIO_URL_FRONTEND?>builder/assets/css/root.css">
    <link rel="stylesheet" href="<?=SITIO_URL_FRONTEND?>builder/assets/css/index.css">



</head>
<body>
<div class="header">
        <i class="fas fa-bars icono sidenav-trigger" data-target="slide-out"></i>
        <div class="titulo <?= ( $isBack ) ? 'back-' : '' ?>titulo-<?=$data['class']?>">
            <a href="./">Duing </a>
        </div>
</div>

<ul id="slide-out" class="sidenav">
  <li>
        <div class="user-view">
            <div class="background">
                <img src="images/perfil2.jpg">
            </div>
        </div>
  </li>    

  <li><a href="./categoria">Categorias</a></li>
  <li><div class="divider"></div></li>

  <li><a href="./clase">Clase</a></li>
  <li><div class="divider"></div></li>

  <li><a href="./controlador">Controlador</a></li>
  <li><div class="divider"></div></li>



  <li><a href="./configuracion"> Configuraciones</a></li>
  <li><div class="divider"></div></li>

  <li><a href="../logout"> Cerrar Sesion</a></li>
</ul>

<div class="container-<?=$data['class']?>">

  <?=$html?>

</div>
    

  <script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
  

  <script src="<?=SITIO_URL_FRONTEND?>builder/assets/js/swiper.min.js"></script>
  <script src="<?=SITIO_URL_FRONTEND?>builder/assets/js/index.js"></script>
  <?php
  if($p =='start') {

   
    echo '<script src="'.SITIO_URL_FRONTEND.'builder/assets/js/start.js"></script>';
  }

?>
</body>
</html>