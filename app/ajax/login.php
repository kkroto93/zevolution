<?php 

session_start();
require_once '../../config.inc.php';
include_once '../../class/Conexion.inc.php';
include_once '../../class/Sesion.php';
include_once '../../class/Modelo.php';
include_once '../../class/Funciones.php';
header('Content-type: application/json');
$arrReturn = array();


if( $_REQUEST['f'] == 'login' ) {

        Funciones::loadClasses('Usuarios');
        global $Usuarios;

        $email = filter_var($_REQUEST['data_form']['email'],FILTER_SANITIZE_STRING);
        $password = filter_var($_REQUEST['data_form']['password'],FILTER_SANITIZE_STRING);

        $usuario = $Usuarios->login($email,$password);

        $arrReturn['errores']  = ( !empty( $usuario['error'] ) ) ? array( $usuario['error'] ) : array();
        $arrReturn['msj'] = $usuario['msj'];
        $arrReturn['status']   = $usuario['status'];
        
        echo json_encode($arrReturn);
        
        
   }

if( $_REQUEST['f'] == 'registro' ) {

        Funciones::loadClasses('Usuarios');
        global $Usuarios;

        $arrErrores = array();
        $status     = false;
        // echo json_encode($_REQUEST);
        // die();

        $email = filter_var($_REQUEST['data_form']['email'],FILTER_SANITIZE_STRING);
        $password = filter_var($_REQUEST['data_form']['password'],FILTER_SANITIZE_STRING);
        $password_rec = filter_var($_REQUEST['data_form']['password-rec'],FILTER_SANITIZE_STRING);

        if($password != $password_rec){
            $arrErrores[] = 'Las contraseñas no coinciden';
        }
        if($Usuarios->getByEmail($email)){
            $arrErrores[] = 'El email ya existe';
        }

        if( count($arrErrores) > 0 ){
            $arrReturn['status'] = $status;
            $arrReturn['errores'] = $arrErrores;
            echo json_encode($arrReturn);
            die();
        }


        $usuario = $Usuarios->registroByEmail($email,$password);

        if($usuario) {

            $status = true;

        }
        $arrReturn['status'] = $status;
        $arrReturn['msj']    = 'Registrado con exito';
        echo json_encode($arrReturn);

        
   }


?>