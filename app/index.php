<?php

session_start();
require      '../vendor/autoload.php';
require_once '../config.inc.php';

$p = 'home';

header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1.
header("Pragma: no-cache"); // HTTP 1.0.
header("Expires: 0"); // Proxies.


if( !empty($_REQUEST['p']) && !empty($_REQUEST['t']) ) {

    $p              =   $_REQUEST['p'];  
    $template_name  =   $_REQUEST['t'];
    
}else{
  $_REQUEST['p'] = $p;
  $template_name  =   $p;
}


Funciones::loadController($p);

$op   = array(
  'extension'       =>  '.html'
);
$options =  array(
  
  'pragmas' => [Mustache_Engine::PRAGMA_BLOCKS],
  'loader'          => new Mustache_Loader_FilesystemLoader(dirname(__FILE__).'/view/templates',$op ) 
  
);


$m = new Mustache_Engine( $options );

$template = $m->loadTemplate($template_name);

$data['@'] = $controller;
$data['app'] = $app;
$data['extra'] = array(

  'checked' => function($disponible, Mustache_LambdaHelper $helper) {

    $disponible = $helper->render($disponible);
    $result = ( $disponible == 1) ? 'disponible_activo' : '';
    return $result;
  }
  
);

$html = $template->render($data);

echo $html;


?>


