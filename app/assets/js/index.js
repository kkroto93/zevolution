
$(function() {

  $('.sidenav').sidenav();
  $('.tabs').tabs();
 
  window.scrollTo(0,document.body.scrollHeight);
  $('.effecto > a').toggleClass('activo');   

  var tmpImg = new Image() ;
  tmpImg.src = $('#imagen-bg').attr('src') ;
  tmpImg.onload = function() {
      // Run onload code.
      $('.effecto > a.activo').addClass('glow');
      
      setTimeout(function() {

          $('.splash-container').addClass('slideOutLeft');
          // $('.splash-container').hide();
          // $('.contenedor-login').fadeIn(1200)


      },2000)
        
     
  }; 


  

  $('.img-show').on('touchstart mousedown', function(e) {

    e.preventDefault();
    console.log('presionando')
    var password =  $(e.currentTarget).prev().prev()[0].value;
    console.log('pass',password)

    $(e.currentTarget).prev().val(password).addClass('show-pass');

    
  });

  $('.img-show').on('touchend mouseup', function(e) {

    e.preventDefault();
    console.log('finalizado la presion')
    $(e.currentTarget).prev().removeClass('show-pass');

    
  });

  $('#formulario-login').submit(function(e){
   
  })

  $('#form-login-submit').click(function(e){
    
    e.preventDefault();
    const form = $('#formulario-login').serializeArray();
    const data_form = getFormData(form);
    const f = getFormStatus();
    console.log('stopeado login data_form',data_form)
    console.log('stopeado login f',f)
    $.post('ajax/login.php',{f,data_form},function(data){

      if(data.status){

        toast(data.msj,'toast-success')
        setTimeout(function(){
          
          window.location.href = '/';

        },1000);

      }else{
        data.errores.forEach(function(error){

          toast(error,'toast-error')

        })
      }
      console.log('data sv responde ',data)

    })




  })

  $('#open-registrar').click(function(e){
      $(this).hide();
      $('.register,.close-registrar').slideDown(500,function(){
        $('.home-text-input.register img').show(); 
      });  

      $('#form-login-submit').val('Registrarse');
      changeFomrStatus('registro')
  })

  $('.close-registrar').click(function(e){

    $(this).hide();
    $('.home-text-input.register img').hide(); 
    $('#open-registrar').show();
    $('.register,.close-registrar').slideUp();  
    $('#form-login-submit').val('Entrar');
    changeFomrStatus('login')
  })

  function getFormStatus() {
    return $('input[name="f"]').val();
  }

  function changeFomrStatus(status) {

    var update = status;

    var f = $('input[name="f"]').val();
    f= update;
    $('input[name="f"]').val(f);
    return $('input[name="f"]').val();

  }
  function toast($texto,$clase) {
    M.toast({
      html: $texto,
      classes:$clase
    });
  }
  
  function getFormData(form) {
    var data = {};
    form.forEach(function (item) {
      var propiedad = item.name;
      var val = item.value;
      data[propiedad] = val;
    })
    return data;
  }




})