<?php


 class Uploader{

    public $file;

    function __construct($file){
        $this->object = $file;
        $this->file= $file;
        return $this->file;
    }

    public function getFile($slug) {

        if(isset( $this->file[$slug] ) && !empty( $this->file[$slug] ) ) {

            $this->file = $this->file[$slug];
            return $this->file;

        }

        return false;
    }

    public function setFile($slug) {
        $this->file = $_FILES[$slug];
        return $this->file;
    }

    function uploadImage($temp,$path) {

            
        return move_uploaded_file($temp,$path);
    }
    
    public function checkFiles() {
    
        $arrReturn = array();
    
        $arrReturn['msg'] = "No se subio niguna imagen";
        $arrReturn['status'] = false;
        $arrReturn['id_error'] = $this->file['error'];
        
        if($this->file['error'] == 0 ) {
    
            $arrReturn['msg'] = "Se recibio la imagen con exito";
            $arrReturn['status'] = true;
            $arrReturn['id_error'] = $this->file['error'];
    
            return $arrReturn;
    
        }
    
        if($this->file['error'] == 4 ) {        
            
            
            return $arrReturn;
    
        }
        return $this->file;
        
    }
    public function createImagePath( $upload_path) {
    
        $nombre_data = explode('.',$this->file['name']);  
       
        $nombre =filter_var( $nombre_data[0] , FILTER_SANITIZE_STRING);
        $ext    =filter_var( $nombre_data[1] , FILTER_SANITIZE_STRING);
    
        $nombre .= '_'.md5( microtime() ); 
    
        $path = $upload_path.$nombre.'.'.$ext;
        return $path;
    }
    
    function getTempFile() {
    
        $temp = filter_var( $this->file['tmp_name'], FILTER_SANITIZE_STRING );
    
        return  $temp;
    }
    
    
    function getName( ) {
    
        $name = explode('.',$this->file['name'])[0];
        
        return $name;
    }
    function getError() {
    
        $error = $this->file['error'] ;
        return $error;
    
    }
    
    function isNewImageImage($upload_path,$new = true) {
    
        $arrReturn  = array();
        $arrReturn['status']    = false;
        $arrReturn['msg']        = 'Esto es para editar';
    
        if( isset($this->file) and !empty($this->file) ) {
    
            //tengo imagenes y proceso $this->file        
            $status = $this->checkFiles($this->file);       
            return $status;
        }
    
        return $arrReturn;
    
    }
}

?>