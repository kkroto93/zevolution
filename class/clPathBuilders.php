<?php

class PathBuilders extends Modelo{


    private $folder_root;
    private $folder_root_slug;
    private $init_folders = array();

    private $init_files = array();
    private $extra_items = array();


    function __construct() {

        Conexion::obtener_conexion();
        parent::__construct('PathBuilder');
        
        $this->folder_root = $this->getRoot();
        $this->init_folders = $this->setFolderPath( $this->getInitFolders() );
        $this->init_files   = $this->setFolderPath( $this->getInitFiles() );        

    }
 
    function setFileContents() {

        include_once 'Controller.php';
        $controller = new Controller();


        $files = $this->getInitFiles();
        $arrEsquemas = array();

        foreach($files as $file ) {

            $slug = $file['slug'];
            $type = $file['esquema_tipo'];
            $path = $this->folder_root.$file['path'];

            //aca tengo el data esquema que luego uso para reemplazar los txt
            $file_esquema = $controller->getTemplateVar($slug,$type);            

            if($type == 'htaccess' || $type == 'layout' || $type == 'login') {
                
                $file_esquema = $controller->getTemplateVar($this->folder_root_slug,$type);
                
            }

            $texto_nuevo_file = $controller->setDefaultContent($path,$type,$file_esquema);                
            $arrEsquemas['init'][] = $texto_nuevo_file;

        }

        return $arrEsquemas; 


    }

    function test() {

        $arrReturn = array();        
        $arrReturn['data'] = $this->crearInit();

        return $arrReturn;


    }

    function crearInitFiles() {

        $files = $this->init_files;
        $new_files = array();
        $arrReturn = array();

        $status     = false;
        $msj        = '';

        foreach ( $files as $folder_path ) {

            $new_file = $this->crearFile($folder_path);

            if($new_file['status']) {

                $status = true;  
                $msj    ='Se crearon los archivos con exito'      ;
            }

        }

        $arrReturn['status']    = $status;
        $arrReturn['msj']       = $msj;

        return $arrReturn;

    }


    function crearInit(){

        
        if( !empty($_REQUEST['slug']) ){
            
            $new_root = $this->crearRootFolder();

        }       

        $new_folders = $this->crearInitFolders();  

        if( $new_folders['status']) {


            $new_files = $this->crearInitFiles();
            if($new_files['status']) {

                $builder_completo   = $this->setFileContents();
                return $builder_completo;         
            }


        }

        
        

    }


    
    function crearRootFolder() {

        $arrReturn  = array();
        $msj        = '';
        $status     = false;      

        $new_folder = $this->crearFolder( $this->folder_root );
    
        if( $new_folder['status'] ) {

            $status     = $new_folder['status'];
            $msj        = $new_folder['msj'];

        }       

        $arrReturn['status'] = $status;
        $arrReturn['msj'] = $msj;

        return $arrReturn;
       
    }

    function setFolderPath($files = array() ) {

        $root = $this->folder_root;
        $arrFilesPath = array();
        
        foreach($files as $file ) {

            $arrFilesPath[] = $root.$file['path'];
        }

        return $arrFilesPath;

    }

    function crearInitFolders() {

        $arrReturn  = array();
        $msj        = '';
        $status     = false;
        
        $folder_creados = $this->loopCrearCarpetas($this->init_folders);       

        if($folder_creados ) {

            return $folder_creados;

        }

    }

    


    function loopCrearCarpetas($files = array() ) {

        $arrReturn  = array();
        $folders    = array();

        $msj        = 'hubo algun error al crear init items';
        $status     = false;
        foreach ( $files as $folder_path ) {

            $new_folder = $this->crearFolder($folder_path);
            $folders[]  = $new_folder;

            if( !$new_folder['status'] ) {

                $status     = $new_folder['status'];
                $msj        = $new_folder['msj'];
                
            }else{
                $status = true;
                $msj = 'se creo con exito: '.$folder_path;
            }

        }

        $arrReturn['status'] = $status;
        $arrReturn['msj'] = $msj;
        $arrReturn['folders'] = $folders;

        return $arrReturn;


    }

    
    private function getRoot() {

        $root = DOCUMENT_ROOT ;

        if( !empty($_REQUEST['slug']) ) {

            $root .= $_REQUEST['slug'].'/' ;
            $this->folder_root_slug = $_REQUEST['slug'];
        }

        return $root;
    }

    function crearFile($path) {
        $arrReturn  = array();
        $msj        = '';
        $status     = false;

        if( !$this->checkFolder($path) ) {

            $handle = fopen($path,'w');

            if(fclose($handle)){
                
                
                $arrReturn['status']    = true;
                $arrReturn['msj']       = $path.' creado con exito';
                return $arrReturn;
            }
        }else{
            $arrReturn['msj'] = ' El '.$type.' ya existe';

        }

        return $arrReturn;
    }

    function crearFolder($path) {

        $arrReturn  = array();
        $msj        = '';
        $status     = false;
        
        if( !$this->checkFolder($path) ) {

            if ( mkdir($path) ) {

                $status = true;
                $msj = 'Se creo el folder: '.$path.' con exito';
    
            }

        }else{

            $msj = "ya existe: ".$path;
        }
        

        $arrReturn['status'] = $status;
        $arrReturn['msj'] = $msj;

        return $arrReturn;



    }


    



    



    ///OBTENGO LOS ARCHIVOS Y CARPETAS DE LA BD PRAA TENER UNA INSTALACION ESTANDAR


    function initFolders(){
        
        return $this->init_folders;

    }
   
    function initFiles(){
        
        return $this->init_files;

    }

    function getInitFolders() {

        $sql = "SELECT * FROM app_filesistem WHERE tipo = 'folder';";
        return Conexion::select($sql);
        
    }

    function getInitFiles(){  


        $sql = "SELECT * FROM app_filesistem WHERE tipo = 'file';";
        return Conexion::select($sql);

    }




    
////CREO EL LISTADDO DE RUTAS PARA CRAR LAS CARPETAS Y ARCHIVOS
    private function crearInitTreeFiles(){

        $list_path = $this->init_files;

        $folder_tree = $this->crearInitTree($list_path);

        return $folder_tree;

    }

    private function crearInitTreeFolders(){

        $list_path = $this->init_folders;

        $folder_tree = $this->crearInitTree($list_path);
        
        return $folder_tree;    

    }

    //ESTA FUNCION CREA EL ARBOL ARCHIVOS
    function crearInitTree($list_path) {

        $item_tree = $this->crearTreeFolder($list_path)['tree'];
        
        if( !$this->isEmpty($item_tree) ) {

            
            return $item_tree;

        }

    }



    private function crearTreeFolder($subfolders = array() , $parent = '' ) {

        $arrTree = array();
        $arrFolderStatus = array();
        $arrReturn  = array();

        if($parent == '' ) {
            $parent = $this->folder_root;
        }
        if( !$this->checkFolder($parent) ){



            foreach($subfolders as $subfolder) {

                $path = $parent.$subfolder;
                $status = 'no existe';
                $arrTree[] = $path  ;

                if( $this->checkFolder($path) ) {

                    $status = true;

                }

                $arrFolderStatus[] = $status;


            }   


        }

        $arrReturn['tree']  = $arrTree;
        $arrReturn['folderStatus']  = $arrFolderStatus;
        return $arrReturn;

    }

    ///TESTEADOS
    private function setRootFolder($path) {

        $folder = DOCUMENT_ROOT.$path.'/';
        $status = false;
        $msj = 'ya existe '.$folder;
        

        if( !file_exists($folder) ) { 

            $status = true;
            $msj = 'exito';
            $this->folder_root = $folder ;
        }

        $arrReturn['msj'] = $msj;
        $arrReturn['path'] = $folder;
        $arrReturn['status'] = $status;

        return $arrReturn;
    }

    function checkFolder( $folder_name )  {

        //test hecho con constante DOCUMENT_ROOT


        $exist = false;

        if( file_exists($folder_name)) {
            $exist = true;
        }

        return $exist;
    
    }

/////////////////////////////////



    

}


?>