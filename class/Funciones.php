<?php

class Funciones {


	public static function setStyles($settings = array()) {

		$filename= '../builder/esquemas/esquema_estilos.txt';

		$saveTo = 'assets/css/estilos.css';
		
		$styles_text  = file_get_contents($filename);

		$toReplace  = array(
			'#color_primario#',
			'#color_secundario#',
			'#color_terciario#',
		);

		$replace = array(
			$settings['color-primario'],
			$settings['color-secundario'],
			$settings['color-terciario'],
		);

		$new_data = str_replace($toReplace,$replace,$styles_text);

		file_put_contents($saveTo,$new_data);

		$new_file  = file_get_contents($saveTo);


	}

    public static function getPartial( $slug, $parent=true ) {

        $ext = '';

        if(!$parent) {

            $ext = '../';            
            
        }

        include_once $ext.$slug.'.php';
	}

	public static function getNombresMeses() {

		$nombres =   ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'];
		return $nombres;

	}
	public static function crearSlugByTitulo( $titulo ) {

	

		 
		
		$palabras 		=  explode(' ',strtolower(self::stripAccents($titulo) ) );
		$slug 			= strtolower( implode('-',$palabras) );
		return $slug;

	}
	static function stripAccents($str) {
		return strtr(utf8_decode($str), utf8_decode('àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ#'), 'aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY ');
	}

	public static function sendMail($nombre, $email, $pay_id, $status){
		
		$to = $email;
		$subject = 'Cuaderno Comprado con exito!'; 
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html; charset=UTF-8\r\n";
		$message = '<html><body>';
		$message .= 
		'
		<h1>Has pagado con exito tu cuaderno!</h1>
		<hr>
		<h2>Muchas gracias por haber realizado esta compra.</h2>
		<h2>Datos de la compra</h2>
		<p><b>Nombre:</b> '.$nombre.'</p>
		<p><b>Email:</b> '.$email.' </p>
		<p><b>ID compra:</b> <code> R$ '.$pay_id.' </code>
		<p><b>Status:</b> '.$status.' </p>
		<hr>
		<p>Podes chequear el estado de tu compra en tu cuenta de mercado pago o ingresando tu id de compra en 
		<a href="'.SITIO_URL_FRONTEND.'/check?id='.$pay_id.'"></a>
		</p>
		</p>';
		$message .= '</body></html>';

		mail($to, $subject, $message, $headers);
	}

    public static function loadClasses($simple=false)
	{
		$arrClass = func_get_args();
		
		foreach($arrClass as $className)
		{

			$classname = '';
			if( gettype($className) != 'array') {

				
				$fileClass = dirname(__FILE__)."/cl".$className.".php";
				$classname = $className;
				
			}
			else{

				
				
				$simple = (bool) $className['simple'];

				$pre=  ($simple) ? "/" : "/cl";
				$fileClass = dirname(__FILE__) .$pre.$className['clase'].".php";
				$classname  = $className['clase'];

			}

			if( is_file($fileClass) ){
					
				require_once($fileClass);
				
				eval('$GLOBALS[\''.$classname.'\'] = new '.$classname.'( );');
				
			}else{
				
				echo "No esta el archivo clase $fileClass <br>";
				
			}


		}
	}
    public static function loadController()
	{	
		
		include_once dirname(__FILE__).'/Controller.php';
		$arrClass = func_get_args();
		foreach($arrClass as $className)
		{
            $fileClass = DOCUMENT_ROOT."controllers/".$className."Controller.php";
			
			if( is_file($fileClass) ){
				
                require_once($fileClass);
                
				eval('global $controller;$controller = new '.$className.'( );');
				
			}else{
				
				echo "No esta el archivo clase $fileClass <br>";
				
			}
		}
	}

	public static function getRequestAction(){
		if ( !empty($_REQUEST['a']) && isset($_REQUEST['a']) ){
			
			$action = filter_var($_REQUEST['a'], FILTER_SANITIZE_STRING);
			return $action;

		}
	}



	public static function generateItems($listado, $class,$callback) {
    
		$html = '';
		
		for ($i=0; $i < count($listado); $i++) { 
			
	
			$html .= $callback($listado[$i],$class);
			
		}
		echo $html;
	}

	
	public static function compress_image($source_url, $destination_url, $quality) {

		$arrReturn 				= array();
		$arrReturn['status'] 	= false;
		$arrReturn['msg'] 		='Hubo algun error';
		$arrReturn['url'] 		= '';

			$info = ( is_file($source_url) ) ? getimagesize($source_url) : false ; 
			if($info) {
				
				
				if ($info['mime'] == 'image/jpeg'){
					
					$image = imagecreatefromjpeg($source_url);
				}		
				elseif ($info['mime'] == 'image/gif') {

					
					
					$arrReturn['status'] 	= true;
					$arrReturn['msg'] 		='exito en la compresion gif';
					$arrReturn['url'] 		= $destination_url;
					return $arrReturn;

				}		
			  	elseif ($info['mime'] == 'image/png'){

					  $image = imagecreatefrompng($source_url);
				  }
				  
				  imagejpeg($image, $destination_url, $quality);
				  

				  
	
				$arrReturn['status'] 	= true;
				$arrReturn['msg'] 		='exito en la compresion';
				$arrReturn['url'] 		= $destination_url;
				
			}
			
			return $arrReturn;

	
		
		
	 }

}

