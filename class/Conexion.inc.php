<?php


class Conexion{

private static $conexion;
private static $status= 'cerrada';

function __construct() {

}

public static function abrir_conexion() {

    if ( !isset(self::$conexion) ) {

    try{
       
        self::$conexion = new PDO('mysql:dbname='.NOMBRE_BD.';'.'host='.NOMBRE_SERVIDOR,NOMBRE_USUARIO,PASSWORD);
        self::$conexion->setAttribute(PDO::ATTR_EMULATE_PREPARES,false);
        self::$conexion->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
        self::$conexion->exec("SET CHARACTER SET utf8");
        self::$status   = 'abierta';

    }
    catch(PDOException $ex){
        print "ERROR".$ex->getMessage().'</br>';
        self::$status   = 'Error de conexion';
        die(); 
    }
    
    }
}
public static function estadoConexion() {

    return self::$status;

}
public static function cerrar_conexion(){

    if( isset(self::$conexion) ) {

        self::$conexion=null;
        self::$status= 'cerrada';

    }

}
public static function obtener_conexion($on = true){
    if($on){

        if( self::$status == 'cerrada' ) {
            
            self::abrir_conexion();
            
            return self::$conexion;
        }
    }else{

        if( self::$status != 'cerrada' ) {

            self::cerrar_conexion();
            return self::estadoConexion();
        }
    }



    return self::$conexion;
}


public static function select($sql, $type = 'all') {

    $stmt   = self::$conexion->prepare($sql);
    $res = $stmt->execute();
    if($res) {

        switch ( $type ) {
            case $type == 'all' :
                return $stmt->fetchAll(PDO::FETCH_ASSOC);

            case $type == 'row' :
            return $stmt->fetch(PDO::FETCH_ASSOC);
            
        }
        
    }
}

public static function lastInserted($tabla){
    
    $sql = "SELECT AUTO_INCREMENT as next FROM INFORMATION_SCHEMA.TABLES 
    WHERE TABLE_SCHEMA = DATABASE() AND TABLE_NAME = '$tabla' ";
    $stm = self::$conexion->prepare($sql);
    $stm->execute();
    return $stm->fetch(PDO::FETCH_ASSOC);
}
public static function delete($sql,$data = array() ) {

    $stm = self::$conexion->prepare($sql);
    return $stm->execute($data);

}
public static function update($sql,$data = array() ) {

    $stmt   = self::$conexion->prepare($sql);
    $res = $stmt->execute($data);
    return $res;
}
public static function insert($sql,$data = array(),$type = 'all') {

    
    $stmt   = self::$conexion->prepare($sql);
    $res = $stmt->execute($data);

    if($res) {

        return self::$conexion->lastInsertId();

    }
    return $res;

    
}


}



?>