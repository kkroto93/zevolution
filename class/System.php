<?php



class System extends Modelo {

    function getObjetos() {
        $sql = "SELECT * FROM objeto WHERE id_app = ".APP_ID;
        return Conexion::select($sql);
    }

    function updateAppName($id, $app_name) {
        
        $id = (int) $id;

        $sql = "UPDATE app_propiedades_valor SET valor = '$app_name' WHERE slug = 'app-name' AND id_app = $id ; ";
        return Conexion::update($sql);

    }

    function getAppSettings() {

        $sql = "SELECT * FROM app_propiedades_valor WHERE id_app = ".APP_ID;
        $settings = Conexion::select($sql);
        $set = array();
        
        foreach($settings as $option ) {

            $set['settings'][ $option['slug'] ] = array( 

                    'valor' => $option['valor'],
                    'id' => $option['id_propiedad']
                    
                );
        }

        return $set['settings'];

        
    }

    function setOpcionesDefault($id_app_nuevo, $app_name='' ) {


        
        $opciones = array();
        $arrReturn = array();
        $status = false;
        $msj = 'Hubo algun error al setear valores default';

        try{


            $sql = "SELECT * FROM app_propiedades_valor  WHERE id_app = 0 ";

            foreach (Conexion::select($sql) as $opcion) {

                unset($opcion['id']);
                unset($opcion['id_app']);
                // unset($opcion['slug']);

                $arrSql = array();
                $arrSql[] = "INSERT INTO app_propiedades_valor SET id_app = $id_app_nuevo ";
    
                foreach(array_keys($opcion) as $key) {
                    
                    $arrSql[] .= " $key = '{$opcion[$key]}' ";


                    
                }
    
                $sql = implode(', ',$arrSql);
    
                Conexion::insert($sql);
                
    
            }

            $status= true;
            $msj = 'Valores Default Seteados con exito';
            // $msj = $sql;

        }catch(Exception $e){
            $msj = $e->getMessage();
        }

        if( $status && !empty($app_name) ) {

           $status =  $this->updateAppName($id_app_nuevo, $app_name);

        }

        $arrReturn['status'] = $status;
        $arrReturn['msj'] = $msj;
        return $arrReturn;

    }

    function test() {
        return 'prendete s';
    }

}

?> 