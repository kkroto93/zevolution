<?php

Funciones::loadClasses(
    array(
        'clase' => 'System',
        'simple' => true
    )
);

global $System;

class App extends Modelo {
    

    private $app_name;
    private $dominio;
    private $app_url;
    
    function __construct() {

        Conexion::obtener_conexion();
        parent::__construct('app');
        
    }

    function setOpcionesValor( $id_app_nuevo ) {

        

            $app_prop_val_nuevo = $System->setOpcionesDefault($id_app_nuevo,$this->app_name);

            return $app_prop_val_nuevo;

    }

    
    
    function createApp($name, $url, $dominio) {


        if ( !empty($name) && !empty($url) && !empty($dominio) ) {

            $nombrecorto = explode('.',$dominio)[0];


            $sql = "INSERT INTO $this->tabla(app_name, app_url, dominio,nombrecorto) VALUES ( ?, ?, ? , ?)";
            
            $insert = array(
                $name,
                $url,
                $dominio,
                $nombrecorto
            );


            $new = Conexion::insert($sql,$insert);

           if($new) {

               $this->app_name = $name;
               $this->dominio = $url;
               $this->app_url = $dominio;

           }



            return $new ;

            
        }
    }
   
    function start($host) {

        global $System;

        $constantes = $this->getByDominio($host);

        if( !$constantes && $_REQUEST['f'] != 'builder' ) {  
        
            echo 'no esta registrado este dominio : '.$host;            
            die(); //un final de la aplicacion.

        }


        define('URL_FRONTEND',$constantes['app_url'].'/');
        define('APP_ID',$constantes['id']);        
        define('APP_SHORTNAME',$constantes['nombrecorto'].'/');
        
        $modulo_settings = $System->getAppSettings();  
        
        $this->setModulos();        
        $this->setStyles($modulo_settings);

        define('APP_NAME',$modulo_settings['app-name']['valor']);
        define('SETTINGS',$modulo_settings);

        return $modulo_settings;


    }

    public function getAppOpcionValor() {

        return SETTINGS;

    }
    public function setStyles($settings = array()) {

        if( $_SERVER['REQUEST_METHOD'] == 'GET' ) {
            
        
            $filename= '../builder/esquemas/esquema_estilos.txt';

            $saveTo = 'assets/css/estilos.css';
            
            $styles_text  = file_get_contents($filename);

            $toReplace  = array(
                '#color_primario#',
                '#color_secundario#',
                '#color_terciario#',
            );

            $replace = array(
                $settings['color-primario']['valor'],
                $settings['color-secundario']['valor'],
                $settings['color-terciario']['valor'],
            );

            $new_data = str_replace($toReplace,$replace,$styles_text);

            file_put_contents($saveTo,$new_data);

            $new_file  = file_get_contents($saveTo);
        }


	}
    function setModulos() {

        $modulos_tipos = $this->getObjetos();

        foreach ( $modulos_tipos as $tipos) {
            define('MODULO_'.strtoupper($tipos['slug']),$tipos['id']);
        }


    }
    
    

    function getUrlFrontend() {
        return 'alguna urls';
    }

    function getApp( $host ) {

        $data = $System->getAppOpcionesByHost($host);

        return $data;


    }

    
    /* construccion del objeto que se replica en la bd */



    function updateByIdPropiedad($id_propiedad, $valor) {

        $id_propiedad   = filter_var($id_propiedad,FILTER_SANITIZE_NUMBER_INT);
        $valor          = filter_var($valor,FILTER_SANITIZE_STRING);

        $sql = "UPDATE app_propiedades_valor SET valor = '$valor' WHERE id_propiedad = $id_propiedad AND id_app = ".APP_ID;
        return Conexion::update($sql);

    }
    

    function getEstadoById($id) {
        $id = filter_var($id,FILTER_SANITIZE_NUMBER_INT);
        $sql = "SELECT slug,id FROM app_estados WHERE id = '$id' AND id_app = ".APP_ID;
        return Conexion::select($sql,'row');
    }
    function getObjetos() {

        $sql = "SELECT * FROM objeto WHERE id_app = ".APP_ID;
        return Conexion::select($sql);

    }

    function getByDominio($url) {


        $url = filter_var($url,FILTER_SANITIZE_STRING);


        $sql = "SELECT * FROM app WHERE dominio = '$url' LIMIT 1;";
        return Conexion::select($sql,'row');

    }

    function getTipos() {

        $sql = "SELECT * FROM app_categoria";
        return Conexion::select($sql);
    }

    function getTipoId( $slug ) {

        $slug = filter_var($slug, FILTER_SANITIZE_STRING);
        
        if ( !empty(APP_ID) ){

            $sql = "SELECT * FROM post_categoria WHERE nombre = '$slug' AND id_app = ".APP_ID;
        }else{
            $sql = "NADA";
        }
        $id_tipo = Conexion::select($sql,'row')['id'];
        return $id_tipo;


    }


  }


    
?>