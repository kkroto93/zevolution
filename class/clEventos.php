<?php

class Eventos extends Modelo{

   
    function __construct() {

        Conexion::obtener_conexion('Evento');
        parent::__construct('evento');

    }

    public function getAll( $orderby='fecha_creacion',$limit = 50 ) {

       
        $sql = "SELECT e.* , ae.slug as estado
                FROM $this->tabla e
                INNER JOIN app_estados ae ON ae.id = e.id_estado
                ORDER BY $orderby DESC LIMIT $limit";
        return Conexion::select($sql);

    }

    function setEntrada($id_entrada, $id_evento) {
        

        Funciones::loadClasses('Entradas');
        global $Entradas;

        $id_entrada = filter_var($id_entrada, FILTER_SANITIZE_NUMBER_INT);

        $hash_entrada = $Entradas->getHashById($id_entrada)['hash_entrada'];        

        $sql = "UPDATE evento SET hash_entrada = '$hash_entrada' WHERE id = '$id_evento' AND id_app = ".APP_ID;

        return Conexion::update($sql);
    }


    function insert( $titulo,$slug, $imagen_principal, $artistas,$id_estado, $fecha, $descripcion='') {

        $sql = "INSERT INTO evento(id_app, titulo, slug, imagen_principal, artistas, id_estado,fecha_evento, descripcion)
                VALUES (? , ?, ? , ? , ? , ?, ? , ?) ";
        $insert = array(
            APP_ID,
            $titulo,
            $slug,
            $imagen_principal,
            $artistas,
            $id_estado,
            $fecha,
            $descripcion
        );
        return Conexion::insert($sql,$insert);

    }

    function update($id,$titulo,$artistas,$id_estado, $fecha,$imagen_principal = '',$descripcion = ''){

        $descripcion = ( $descripcion == '' ) ? '' : ', descripcion =" '.$descripcion.'"';
        $imagen_principal = ( $imagen_principal == '' ) ? '' : ', imagen_principal ="'.$imagen_principal.'"';
        $sql = "UPDATE evento  SET  titulo = ? , artistas = ? , id_estado = ?, fecha_evento = ? ".$descripcion.$imagen_principal;
        $sql .= "WHERE id = '$id' AND id_app = ".APP_ID;
        $update = array(
            $titulo,
            $artistas,
            $id_estado,
            $fecha
        );
        return Conexion::update($sql,$update);
    }
    function getEventos() {

        $sql = "SELECT * FROM evento WHERE id_app = ". APP_ID;
        return Conexion::select($sql);
        
    }

    



}


?>