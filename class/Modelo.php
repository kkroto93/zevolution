<?php


class Modelo extends Dobjects{

    


    function __construct($tabla='') {
        
           
        $this->tabla = $tabla;       
    }

    
    public function getAll( $orderby='fecha_creacion',$limit = 50 ) {

       
        $sql = "SELECT * FROM $this->tabla ORDER BY $orderby DESC LIMIT $limit";
        return Conexion::select($sql);

    }


    function getHashById($id) {

        $id = filter_var($id, FILTER_SANITIZE_NUMBER_INT);

        $sql = "SELECT hash_entrada FROM $this->tabla WHERE id = $id AND id_app = ".APP_ID;
        return Conexion::select($sql,'row');

    }

    function delete ( $id ) {

        $sql = "DELETE FROM $this->tabla WHERE id = '$id' AND id_app = ".APP_ID;
        return Conexion::delete($sql);

    }

    function get( $id ) {

        $id = filter_var($id,FILTER_SANITIZE_NUMBER_INT);

        $sql = "SELECT * FROM $this->tabla WHERE id = $id LIMIT 1";
        $tela = Conexion::select($sql,'row');
        return $tela;


    }
    function getAllWithEstado() {

        $sql = "SELECT t.*, e.slug as estado_actual FROM $this->tabla t
                INNER JOIN app_estados e ON e.id = t.id_estado
                AND t.id_app = ".APP_ID;
        return Conexion::select($sql);
        
    }
    function getById( $id ) {

        $id = filter_var($id,FILTER_SANITIZE_NUMBER_INT);

        $sql = "SELECT * FROM $this->tabla WHERE id = '$id' AND id_app = ".APP_ID." LIMIT 1";
        
        return Conexion::select($sql,'row');


    }


    function getNext($tabla) {

        return Conexion::lastInserted($tabla)['next'];

    }

    function  setDisponible($id, $disp,$tabla ) {

        $sql = "UPDATE $this->tabla SET disponible = ? WHERE id = $id ";

        $update = array(
            $disp
        );

        return Conexion::update($sql,$update);

    }

    function getDisponibles ($orderby,$limit = 50) {

        $sql = "SELECT * FROM $this->tabla WHERE disponible = 1 AND id_app =  ORDER BY $orderby DESC LIMIT $limit";

        return Conexion::select($sql);
    }

    function getEstados() {
        
        $modulo = eval(' return MODULO_'.strtoupper($this->tabla).';') ;

        $sql = "SELECT * FROM app_estados WHERE id_modulo_b_id = '$modulo' AND id_app = ".APP_ID;
        return Conexion::select($sql);
    }

    function resetHash($id_entrada) {

        $sql = "SELECT id_evento 
                FROM entrada 
                WHERE id = '$id_entrada' AND id_app = ".APP_ID;

        $id_evento = Conexion::select($sql,'row');
        $id_evento = (int)$id_evento['id_evento'];
        // return $id_evento;
        $sql = "UPDATE evento SET hash_entrada = ' ' WHERE id = '$id_evento' AND id_app = ".APP_ID;
        return Conexion::update($sql);

    }

    function deleteImagen($id) {

        $status = false;
        $msj    = 'hubo error al eliminar la imagen';



        $objeto = $this->getById($id);
        $path_imagen = DOCUMENT_ROOT.$objeto['imagen_principal'];
        $deleted = unlink($path_imagen);
        
        if ($deleted ) {

            $status = true;
            $msj = 'Se elimino el adjunto con exito';
            
        }
        
        $arrReturn['status'] = $status;
        $arrReturn['msj'] = $msj;

        return $arrReturn;
    }

}

?>