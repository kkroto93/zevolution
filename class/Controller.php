<?php

class Controller extends Sesion{

    private $css_class_name;
    private $filename_controller ;
    private $filename_class ;
    private $filename_template ;
    private $filename_builder ;
    private $filename_htaccess ;
    private $settings;
    function __construct($class='') {
        
        Conexion::obtener_conexion();

        //  $this->settings = SETTINGS;
         $this->css_class_name      = $class;
         $this->filename_controller = DOCUMENT_ROOT.'controllers/';
         $this->filename_class      = dirname(__FILE__).'/cl';
         $this->filename_template   = 'view/templates/';
         $this->filename_builder    = DOCUMENT_ROOT.'builder/esquemas/esquema_';
         $this->filename_htaccess    = '.htaccess';
        
    }
    function init(){
        
        return 'init';

    }
    function getMes($indice){
        $meses = ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'];
        return $meses[$indice - 1];
    }
    function actionSwitch() {

        $arrReturn['status']    = false;
        $arrReturn['error']     = 'Hubo algun error en actionSwitchHtaccess';   

        $action     = filter_var($_REQUEST['a'],FILTER_SANITIZE_STRING);
        $type       = filter_var($_REQUEST['t'],FILTER_SANITIZE_STRING);;
        $slug       = filter_var($_REQUEST['slug'],FILTER_SANITIZE_STRING);
        $root       = ( !empty( $_REQUEST['f'] ) ? '../'.$_REQUEST['f'].'/' : '../');

        if( $action == 'crear'){

            $method_action = 'crear'. ucfirst($type);

            return $this->$method_action($slug, $type,$root);

        }

        if( $action == 'delete') {

            $method_action = 'delete'.ucfirst($type);
            return $this->$method_action($slug, $type, $root);

        }
        
        
        

        return $arrReturn;
    }

    function crearAbm($slug, $type, $root ) {
        $slug = $slug.'_abm';
        return $this->crearFile($slug, $type, $root);

    }
    function deleteAbm($slug, $type, $root) {

        $filename= $root.$this->filename_template.$slug.'_abm.html';
        return $this->deleteFile($filename);

    }

    function crearTemplate($slug, $type, $root) {

           
        return $this->crearFile($slug, $type, $root);

    }
    function crearFile($slug, $type, $root) {

        $file_esquema    = $this->getTemplateVar($slug,$type);
        $file            = $this->crearEsquema($slug, $file_esquema, $type, $root);
        return $file;

    }

    function deleteTemplate($slug, $type, $root) {
        
        $filename= $root.$this->filename_template.$slug.'.html';
        return $this->deleteFile($filename);
        
    }
    

    //////////////CRUD CONTROLLER

    function crearControl($slug,$type,$root) {

        $control_esquema    = $this->getTemplateVar($slug,$type);
        $control            = $this->crearEsquema($slug, $control_esquema, $type, $root);
        return $control;
    }

    function deleteControl($slug, $type, $root) {

        $filename= $this->filename_controller.$slug.'Controller.php';
        return $this->deleteFile($filename);
    }


    ///////CRUD CLASE
    function crearClase($slug,$type, $root ) {


        $clase_esquema  = $this->getTemplateVar($slug,$type);       
        $clase = $this->crearEsquema($slug,$clase_esquema,$type,$root);
        return $clase;
        
    }

    function deleteClase($slug, $type, $root) {

        $filename = $this->filename_class.$slug.'s.php';
        return $this->deleteFile($filename);
        
    }


    /////////////////////////CRUD HTACCESS
    function crearHtaccess($slug, $type,$root){

        $htaccess_esquema       = $this->getTemplateVar($slug,$type);
        $htaccess               = $this->crearEsquema($slug,$htaccess_esquema,$type,$root);
        return $htaccess;

    }
    function deleteHtaccess($slug, $type, $root) {


        $filename = $root.'.htaccess';
        return $this->deleteFile($filename);


    }

    /////CRUD PACK
    function crearPack($slug, $type, $root) {

        $arrReturn['status']    = false;
        $arrReturn['error']     = 'Hubo algun error';       
        
      

            
            $clase_esquema  = $this->getTemplateVar($slug,'clase');
            $controller_esquema  = $this->getTemplateVar($slug,'control');
            $template_esquema  = $this->getTemplateVar($slug,'template');
            
            $clase = $this->crearEsquema($slug,$clase_esquema,'clase',$root);
            $controller = $this->crearEsquema($slug,$controller_esquema,'control',$root);
            $template = $this->crearEsquema($slug,$template_esquema,'template',$root);

            if($clase['status'] && $controller['status'] && $template['status'] ) {

                $sql = "INSERT INTO objeto(slug,id_app) VALUES (?,?)  ";
                $insert = array(
                    $slug,
                    APP_ID
                );
                
                $exito = Conexion::insert($sql,$insert);
                
                $arrReturn['error'] = '';
                $arrReturn['status'] = $exito;


               
            }
                $arrReturn['pack'][] = $clase;
                $arrReturn['pack'][] = $controller;
                $arrReturn['pack'][] = $template;
            
            return $arrReturn; 
              

    }


    function deletePack($slug,$type, $root){

        $arrReturn              = array();
        $slug_clase             = $slug.'s';
        $arrReturn['pack']['status']    = false;
        $arrReturn['pack']['msj']       = 'Los siguientes archivos ya no existen : ';
        $delete_class           = false;
        $delete_controller      = false;
        $delete_template        = false;

        //borro clase
        $filename_class         = $this->filename_class.ucfirst($slug_clase).'.php';        
        if ( file_exists($filename_class) ){
            
            $delete_class           = unlink($filename_class );

        }else{
            $arrReturn['pack']['msj'] .= PHP_EOL.$filename_class;
        }

        $filename_controller    = $this->filename_controller.$slug.'Controller.php';        
        if (file_exists($filename_controller)){
            //borro controller
            $delete_controller      = unlink($filename_controller );

        }else{
            $arrReturn['pack']['msj'] .= PHP_EOL.$filename_controller;
        }  
        
        $filename_template      = $root.$this->filename_template.$slug.'.html';
        if(file_exists($filename_template)) {
            //borro template
            $delete_template        = unlink($filename_template);

        }else{
            $arrReturn['pack']['msj'] .= PHP_EOL.$filename_template;
        } 


        if($delete_class && $delete_controller && $delete_template ) {

            $arrReturn['pack']['status'] = true;
            $arrReturn['pack']['msj'] = 'Se borro el pack con exito';
            
        }

        return $arrReturn;
   }



   //////////////CREACION ESQUEMAS   

    function crearEsquema($slug='',$data_esquema=array(),$type='',$folder=''){

        $arrReturn = array();

        $arrReturn['status']    = false;
        $arrReturn['error']     = '';
        $filename               = '';

        if( $type == 'control') {

            $filename = $this->filename_controller.$slug.'Controller.php';
        }

        if($type == 'clase') {

            $filename =$this->filename_class.ucfirst($slug).'s.php';
        }

        if($type == 'template') {

            
            $filename = $folder.$this->filename_template.$slug.'.html';

        }
        if($type == 'layout') {

            $filename = $folder.$this->filename_template.'layouts/default_layout.html';
        }

        if($type == 'htaccess'){

            $filename = $folder.'.htaccess';

        }

        if($type == 'index') {

            $filename = $folder.$slug.'.php';
        }

        if( $type == 'css' || $type == 'css_root') {

            $filename = $folder.$slug.'.css';

        }

        if( $type == 'js' ) {

            $filename = $folder.$slug.'.js';

        }

        if ( $type == 'abm' ) {

            $filename = $folder.$this->filename_template.$slug.'.html';
        }

        if( !file_exists($filename) && !empty($filename) ) {

            $handle = fopen($filename,'w');
            $layout = file_get_contents($this->filename_builder.$type.'.txt');
            $new_esquema_text = $layout;
            if( !empty($data_esquema['data_replace']) && isset($data_esquema['data_replace']) ) {

                $data = $data_esquema['data_replace'];

                $replace = $data_esquema['replace'];
    
                $new_esquema_text = str_replace($replace,$data,$layout);

            }
            
            fwrite($handle,$new_esquema_text);
            if(fclose($handle)){
                
                $arrReturn['status']    = true;
                $arrReturn['objeto']    = array(
                        'type' => $type,
                        'slug' =>$slug,
                        'root' =>$folder
                );
                $arrReturn['msj']       = $type.' creado con exito';
                return $arrReturn;
            }
    
            
        }else{

            $arrReturn['error'] = ' El '.$type.' ya existe';

        }        
        
        return $arrReturn;

        
   }

   function setDefaultContent($file_path,$esquema_tipo,$data_esquema) {

    $arrReturn = array();

    $status     = false;
    $msj        = 'hubo algun error al setear el contenido default';

    $esquema_texto  = file_get_contents($this->filename_builder.$esquema_tipo.'.txt');

    $data = $data_esquema['data_replace'];
    $replace = $data_esquema['replace'];
    $new_content = str_replace($replace,$data,$esquema_texto);

    if( file_put_contents($file_path,$new_content) ) {

        $status = true;
        $msj    = 'Se inserto el contenido defaul con exito en: '.$file_path;

    }

    $arrReturn['status']    = $status;
    $arrReturn['msj']       = $msj;

    return $arrReturn;



   }

   function getTemplateVar($slug,$type) {


    $data_replace   = array();
    $replace        = array();
    $data_esquema   = array();

    if($type == 'control') {

        $data_replace = array(
            $slug,//perro
            ucfirst($slug)//Perro
        );
        $replace = array(
            '#nombre_controller#',
            '#nombre_clase#'
        );

    }

    if($type == 'htaccess' ) {

        $data_replace = array(
            $slug //back
        );
        $replace = array(
            '#ROOT#'
        );

    }
    if( $type == 'layout' ) {

        $data_replace = array(
            $slug //back
        );
        $replace = array(
            '#ROOT#'
        );

    }

    if( $type == 'login' ) {

        $data_replace = array(
            $slug //back
        );
        $replace = array(
            '#ROOT#'
        );

    }


    if($type == 'clase') {

        $data_replace = array(
            ucfirst( $slug),
            $slug
        );
        $replace = array(
            '#nombre_clase#',
            '#nombre_tabla#'
        );

    }
    if($type == 'template') {

        $data_replace = array(            
            'pagina De template test',
            ucfirst( $slug)
        );
        $replace = array(            
            '#titulo_pagina#',
            '#clase_template#'
        );
    }

    $data_esquema['data_replace']   = $data_replace;
    $data_esquema['replace']        = $replace;

    return $data_esquema;

}

function deleteFile($filename) {

    $arrReturn['status']    = false;
    $arrReturn['error']     = 'Hubo algun error'; 
    if(file_exists($filename)){

        $delete_file  = unlink($filename);
        
        if($delete_file) {
    
            $arrReturn['status']    = $delete_file;
            $arrReturn['error']     = 'Se elimino '.$filename.' con exito'; 
    
        }
    }else{
        $arrReturn['error'] = 'El archivo ya no existe';
    }

    return $arrReturn;
    



}



    public function sayHi(){
        echo 'hi perra';
    }

    function getData() {
        
    }

    function getClassName() {

        return $this->css_class_name; 

    }
    function get_app_name(){
        return APP_NAME;
    }

    function url_frontend() {
        return SITIO_URL_FRONTEND;
    }
    function get_p(){
        return $_REQUEST['p'];
    }

    function tipo_abm(){

        $type = 'nuevo';

        if( !empty($_REQUEST['id']) ){
            
            $type = 'editar';

        }

        return $type;

    }
    function tipo_post_abm() {
        
        if( !empty($_REQUEST['a']) && $_REQUEST['a'] == 'crear' ) {

            $slug =strtolower(trim(  $_REQUEST['p'] ));
            Funciones::loadClasses('App');
            global $App;
            $id_tipo = $App->getTipoId($slug);
            return $id_tipo;

        }
    }
    function fomularioIdName() {

        if(!empty($_REQUEST['slug'])){
            
            return filter_var($_REQUEST['slug'],FILTER_SANITIZE_STRING);

        }
    }
    function getLastId() {

        Funciones::loadClasses('Eventos');
        global $Eventos;
        return $Eventos->getNext('evento') ;


    }
    function getAppInfo() {

        Funciones::loadClasses('App');
        global $App;

        $app = $App->getInfo();
        

    }
    function getRandomDay() {
        return mt_rand(1,31);

    }

    function getColorPrimario() {

        if ( !empty($this->settings) ){

            return $this->settings['color-primario'];
        }
        


    }
    function getColorSecundario() {

        if ( !empty($this->settings) ){

            return $this->settings['color-secundario'];
        }
    }
    function getColorTerciario() {

        if ( !empty($this->settings) ){

            return $this->settings['color-terciario'];
        }
    }


    function getRandomMes() {
        $meses = ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'];
        $random = mt_rand(0,count($meses)-1);
        $mes = $meses[$random];
        return $mes;
    }
    function getTextoBoton() {
        
        return strtoupper($this->tipo_abm());

    }

    function get_request_id(){

        $id=0;

        if( !empty($_REQUEST['id']) ){
            
            $id = $_REQUEST['id'];

        }

        return $id;
    }

    
    

}

?>