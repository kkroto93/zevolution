<?php


class Usuarios extends Modelo{

    function __construct() {

        Conexion::obtener_conexion();
        parent::__construct('usuario');
        
    }

    function getByEmail( $email ){

        $email = filter_var($email,FILTER_SANITIZE_STRING);

        $sql = "SELECT * FROM usuario WHERE email = '$email' LIMIT 1; ";

        return Conexion::select($sql,'row');

    }

    function insert($nombre, $apellido, $email , $password) {

        $sql = "INSERT INTO usuario (nombre,apellido,email,password) VALUES (?,?,?,?) ";

        $insert = array(
            $nombre,
            $apellido,
            $email,
            md5(md5($password))
        );

        return Conexion::insert($sql,$insert);

    }

    function registroByEmail($email,$password){

        $hash_pass  = md5(md5($password));
        $nombre = explode('@',$email)[0];
        $sql = "INSERT INTO usuario SET 
                nombre = '$nombre',
                email = '$email',
                password = '$hash_pass',
                privilegio = 'regular',
                id_app = ".APP_ID." ; " ;

        return Conexion::insert($sql);
    }

    
    function login($email, $password) {
        
        $arrReturn  = array();
        $status     = false;
        $error      = 'Accesso incorrecto';
        $msj        = '';

        $hash_pass  = md5(md5((int)$password));
        $usuario    = $this->getByEmail($email);
        
        $check_pass  =  '';

        if( !empty($usuario['password'] ) ) {

           $check_pass =  preg_match('/'.$usuario['password'].'/',$hash_pass);


        }


        if( $check_pass && $check_pass != '' ) {

            $error      = '';

            $sesion = Sesion::start($email);

            $status     = $sesion['status'];
            $msj        = $sesion['msj'];
            $arrReturn['sesion'] = $sesion;
             

        }
        
        $arrReturn['status']    = $status;
        $arrReturn['msj']       = $msj;
        $arrReturn['error']     = $error;


        return $arrReturn;
        



    }

}


?>