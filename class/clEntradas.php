<?php

class Entradas extends Modelo{

   
    function __construct() {

        Conexion::obtener_conexion('Entrada');
        parent::__construct('entrada');

    }




    
    function update($id, $id_estado, $id_evento , $nombre, $imagen_principal ='' ) {

        $imagen_principal = ( $imagen_principal == '' ) ? '' : ', imagen_principal ="'.$imagen_principal.'"';
        $sql = "UPDATE $this->tabla  SET  id_estado = ? , id_evento = ? , nombre = ? " .$imagen_principal;
        $sql .= "WHERE id = '$id' AND id_app = ".APP_ID;
        $update = array(
            $id_estado,
            $id_evento,
            $nombre
        );

        $this->getAll();
        return Conexion::update($sql,$update);
    }

    function getEventosNuevos() {

        $sql = "SELECT * FROM evento WHERE id_app =".APP_ID;
        return Conexion::select($sql);

    }

    function checkHashEntrada() {

    }
    function insert($id_estado, $id_evento, $slug, $nombre, $imagen_principal ){

        
        $sql = "INSERT INTO entrada(id_app, id_evento, id_estado, slug, nombre, imagen_principal,hash_entrada)
                VALUES(?,?,?,?,?,?,?) ";

        $insert = array(
            APP_ID,
            $id_evento,
            $id_estado,
            $slug,
            $nombre,
            $imagen_principal
        );

        $hash_entrada = $this->crearHashEntrada($insert);
        array_push($insert,$hash_entrada);
        return Conexion::insert($sql,$insert);
    }
    function getHashById($id) {

        $id = filter_var($id, FILTER_SANITIZE_NUMBER_INT);

        $sql = "SELECT hash_entrada FROM entrada WHERE id = $id AND id_app = ".APP_ID;
        
        return Conexion::select($sql,'row');

    }
    function crearHashEntrada($data = array() ) {

        $hash = '' ;

        foreach ( $data as $item ) {

            $hash .= $item.'+';
        }

        $hash = md5( $hash.md5( time() ) );

        return $hash;

    }
}


?>