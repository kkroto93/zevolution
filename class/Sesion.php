<?php 

class Sesion {

    private static $email;
    private static $sesion;
    private static $sesion_data;

    function __construct(){

    }

    static function crear() {

        Funciones::loadClasses('Usuarios');
        global $Usuarios;
        

        $arrReturn = array();
        $status = false;
        $msj = 'Hubo algun error al logearse';

        $usuario = $Usuarios->getByEmail(self::$email);

        if($usuario) {

            self::$sesion_data = $usuario;
            self::$sesion = true;
            self::login();

            $msj = "Logeado con exito";
            $status = true;

        }
        $arrReturn['msj'] = $msj;
        $arrReturn['status'] = $status;

        return $arrReturn;
            
    }

    static function login() {

        $status= false;
        $data = array();

        $estoylogueado = false;
        //si no (!) estoylogueado
        if ( !$estoylogueado ) {
            //esta logueado?
            // se tiene que logear
            $data = self::$sesion_data;
            

            $_SESSION['user']['data']       = $data;
            $_SESSION['login']['status']    = self::$sesion;


        }else{
            //aca se tiene que deslogear
            session_destroy();

        }    
            

    }

   static function start($email) {

        $arrReturn = array();

        $status = false;
        $msj  = 'Hubo algun error al start sesion';

        if( !empty($email) && $email != '') { 
            
            
            self::$email = $email;
             $crear = self::crear();
             return $crear;
        }

        return $arrReturn;
    }

    static function checkLogin() {

        $login = false;

        if( !empty( $_SESSION['login']['status'] ) && $_SESSION['login']['status'] ){
            
            $login = true;

        }

        return $login;


    }


}



?>