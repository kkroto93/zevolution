-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.1.39-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win64
-- HeidiSQL Versión:             10.1.0.5464
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Volcando estructura para tabla kwsys.app
CREATE TABLE IF NOT EXISTS `app` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_name` varchar(50) NOT NULL,
  `nombrecorto` varchar(510) NOT NULL,
  `app_url` varchar(300) NOT NULL,
  `dominio` varchar(250) NOT NULL,
  `fechaAlta` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla kwsys.app: ~6 rows (aproximadamente)
DELETE FROM `app`;
/*!40000 ALTER TABLE `app` DISABLE KEYS */;
INSERT INTO `app` (`id`, `app_name`, `nombrecorto`, `app_url`, `dominio`, `fechaAlta`) VALUES
	(23, 'Ferchala Cuadernos', '', 'http://mvc.duing.com', 'mvc.duing.com', '2019-05-24 23:51:50'),
	(24, 'Aplicacion de prueba', '', 'http://kory.thet.com.ar/duing-mvc/', 'kory.thet.com.ar', '2019-05-28 13:25:28'),
	(30, 'Ferchala Cuadernos', '', 'http://ferchala.duing.com', 'ferchala.duing.com', '2019-06-02 23:18:05'),
	(34, 'TechnoParty Gb', 'technoparty', 'http://technoparty.duing.com', 'technoparty.duing.com', '2019-06-16 20:09:51'),
	(35, 'TechnoParty Gb', 'technoparty', 'https://192.168.0.143', '192.168.0.143', '2019-06-16 20:09:51'),
	(36, 'TechnoParty Gb', 'technoparty', 'https://192.168.0.28', '192.168.0.28', '2019-06-16 20:09:51');
/*!40000 ALTER TABLE `app` ENABLE KEYS */;

-- Volcando estructura para tabla kwsys.app_categoria
CREATE TABLE IF NOT EXISTS `app_categoria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_app` int(11) NOT NULL,
  `nombre` varchar(150) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla kwsys.app_categoria: ~0 rows (aproximadamente)
DELETE FROM `app_categoria`;
/*!40000 ALTER TABLE `app_categoria` DISABLE KEYS */;
/*!40000 ALTER TABLE `app_categoria` ENABLE KEYS */;

-- Volcando estructura para tabla kwsys.app_estados
CREATE TABLE IF NOT EXISTS `app_estados` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_app` int(11) NOT NULL DEFAULT '0',
  `id_modulo_b_id` int(11) NOT NULL,
  `slug` varchar(250) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla kwsys.app_estados: ~9 rows (aproximadamente)
DELETE FROM `app_estados`;
/*!40000 ALTER TABLE `app_estados` DISABLE KEYS */;
INSERT INTO `app_estados` (`id`, `id_app`, `id_modulo_b_id`, `slug`) VALUES
	(1, 34, 23, 'no-disponible'),
	(2, 34, 23, 'venta'),
	(3, 34, 23, 'disponible'),
	(4, 34, 23, 'edicion-limitada'),
	(5, 34, 23, 'finalizada'),
	(6, 34, 22, 'pendiente'),
	(7, 34, 22, 'cancelado'),
	(8, 34, 22, 'ahora'),
	(9, 34, 22, 'borrador'),
	(10, 34, 22, 'publicado');
/*!40000 ALTER TABLE `app_estados` ENABLE KEYS */;

-- Volcando estructura para tabla kwsys.app_filesistem
CREATE TABLE IF NOT EXISTS `app_filesistem` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo` enum('folder','file') NOT NULL,
  `esquema_tipo` enum('index','htaccess','clase','control','template','pack','layout','js','css','css_root') DEFAULT NULL,
  `path` varchar(250) NOT NULL,
  `slug` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla kwsys.app_filesistem: ~15 rows (aproximadamente)
DELETE FROM `app_filesistem`;
/*!40000 ALTER TABLE `app_filesistem` DISABLE KEYS */;
INSERT INTO `app_filesistem` (`id`, `tipo`, `esquema_tipo`, `path`, `slug`) VALUES
	(1, 'folder', 'index', 'assets', 'assets'),
	(2, 'folder', 'index', 'assets/css', 'css'),
	(3, 'folder', 'index', 'assets/js', 'js'),
	(4, 'folder', 'index', 'assets/images', 'images'),
	(5, 'folder', 'index', 'view', 'view'),
	(6, 'folder', 'index', 'view/templates', 'template'),
	(7, 'folder', 'index', 'view/templates/partials', 'template-partials'),
	(8, 'file', 'index', 'index.php', 'index'),
	(9, 'file', 'htaccess', '.htaccess', 'htaccess'),
	(10, 'file', 'template', 'view/templates/home.html', 'home'),
	(11, 'file', 'layout', 'view/templates/layouts/default_layout.html', 'default_layout'),
	(12, 'folder', 'layout', 'view/templates/layouts', 'layouts'),
	(13, 'file', 'js', 'assets/js/index.js', 'index-js'),
	(14, 'file', 'css', 'assets/css/index.css', 'index'),
	(15, 'file', 'css_root', 'assets/css/root.css', 'root');
/*!40000 ALTER TABLE `app_filesistem` ENABLE KEYS */;

-- Volcando estructura para tabla kwsys.app_propiedades
CREATE TABLE IF NOT EXISTS `app_propiedades` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_propiedad_tipo` int(11) NOT NULL,
  `tipo` enum('color','input','textarea','select','contenido','text','boolean') NOT NULL,
  `slug` varchar(50) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `orden` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla kwsys.app_propiedades: ~8 rows (aproximadamente)
DELETE FROM `app_propiedades`;
/*!40000 ALTER TABLE `app_propiedades` DISABLE KEYS */;
INSERT INTO `app_propiedades` (`id`, `id_propiedad_tipo`, `tipo`, `slug`, `nombre`, `orden`) VALUES
	(1, 1, 'text', 'app-name', 'Nombre Aplicacion', 0),
	(2, 1, 'color', 'color-principal', 'Color principal', 0),
	(3, 1, 'color', 'color-secundario', 'Color secundario', 0),
	(4, 1, 'color', 'color-terciario', 'Color terciario', 0),
	(5, 1, 'text', 'integracion-mp-id', 'MercadoPago ID', 0),
	(6, 1, 'text', 'integracion-mp-secret', 'MercadoPago SECRET', 0),
	(7, 1, 'boolean', 'app-contenido-blog', 'Blog', 0),
	(8, 1, 'boolean', 'app-contenido-tienda', 'Tienda', 0);
/*!40000 ALTER TABLE `app_propiedades` ENABLE KEYS */;

-- Volcando estructura para tabla kwsys.app_propiedades_tipo
CREATE TABLE IF NOT EXISTS `app_propiedades_tipo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `orden` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla kwsys.app_propiedades_tipo: ~4 rows (aproximadamente)
DELETE FROM `app_propiedades_tipo`;
/*!40000 ALTER TABLE `app_propiedades_tipo` DISABLE KEYS */;
INSERT INTO `app_propiedades_tipo` (`id`, `nombre`, `orden`) VALUES
	(1, 'Sitio', 0),
	(2, 'Diseño', 0),
	(3, 'Integraciones', 0),
	(4, 'Perfil', 0);
/*!40000 ALTER TABLE `app_propiedades_tipo` ENABLE KEYS */;

-- Volcando estructura para tabla kwsys.app_propiedades_valor
CREATE TABLE IF NOT EXISTS `app_propiedades_valor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_app` int(11) NOT NULL,
  `id_propiedad` int(11) NOT NULL,
  `valor` varchar(555) NOT NULL,
  `slug` varchar(555) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=97 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla kwsys.app_propiedades_valor: ~6 rows (aproximadamente)
DELETE FROM `app_propiedades_valor`;
/*!40000 ALTER TABLE `app_propiedades_valor` DISABLE KEYS */;
INSERT INTO `app_propiedades_valor` (`id`, `id_app`, `id_propiedad`, `valor`, `slug`) VALUES
	(1, 0, 1, 'Bienvenido-Default', 'app-name'),
	(2, 0, 2, '#000000', 'color-primario'),
	(3, 0, 3, '#000000', 'color-secundario'),
	(4, 0, 4, '#000000', 'color-terciario'),
	(5, 0, 5, 'teset-id', 'mp-id'),
	(6, 0, 6, 'secret-test', 'mp-secret'),
	(7, 34, 1, 'TechnoClubazox', 'app-name'),
	(8, 34, 2, '#ff0000', 'color-primario'),
	(9, 34, 3, '#ffff80', 'color-secundario'),
	(10, 34, 4, '#00ff00', 'color-terciario'),
	(11, 34, 5, 'nombre vago', 'mp-id'),
	(12, 34, 6, 'vaguisimo', 'mp-secret');
/*!40000 ALTER TABLE `app_propiedades_valor` ENABLE KEYS */;

-- Volcando estructura para tabla kwsys.entrada
CREATE TABLE IF NOT EXISTS `entrada` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_app` int(11) NOT NULL,
  `id_evento` int(11) NOT NULL,
  `id_estado` int(11) NOT NULL,
  `hash_entrada` varchar(250) NOT NULL,
  `slug` varchar(510) NOT NULL,
  `nombre` varchar(250) NOT NULL,
  `imagen_principal` varchar(250) NOT NULL,
  `imagen_qr` varchar(250) NOT NULL,
  `fecha_creacion` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_modificacion` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fecha_expiracion` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla kwsys.entrada: ~0 rows (aproximadamente)
DELETE FROM `entrada`;
/*!40000 ALTER TABLE `entrada` DISABLE KEYS */;
INSERT INTO `entrada` (`id`, `id_app`, `id_evento`, `id_estado`, `hash_entrada`, `slug`, `nombre`, `imagen_principal`, `imagen_qr`, `fecha_creacion`, `fecha_modificacion`, `fecha_expiracion`) VALUES
	(22, 34, 47, 2, '608f503697bc600dce466e22aa97aff5', 'para-refrescar-la-semana', 'Para refrescar la semana no?', 'uploads/technoparty/entrada/techno1_f8226e7879d2f144a6fe277660e2485d.gif', '', '2019-06-20 23:27:01', '2019-06-20 23:27:24', '0000-00-00 00:00:00');
/*!40000 ALTER TABLE `entrada` ENABLE KEYS */;

-- Volcando estructura para tabla kwsys.entrada_valida
CREATE TABLE IF NOT EXISTS `entrada_valida` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_usuario` int(11) NOT NULL,
  `hash_entrada_valido` varchar(150) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla kwsys.entrada_valida: ~0 rows (aproximadamente)
DELETE FROM `entrada_valida`;
/*!40000 ALTER TABLE `entrada_valida` DISABLE KEYS */;
/*!40000 ALTER TABLE `entrada_valida` ENABLE KEYS */;

-- Volcando estructura para tabla kwsys.evento
CREATE TABLE IF NOT EXISTS `evento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_app` int(11) NOT NULL,
  `id_estado` int(11) NOT NULL,
  `hash_entrada` varchar(150) NOT NULL,
  `titulo` varchar(150) NOT NULL,
  `slug` varchar(150) NOT NULL,
  `imagen_principal` varchar(250) NOT NULL,
  `artistas` varchar(150) NOT NULL,
  `descripcion` varchar(250) NOT NULL,
  `fecha_creacion` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_modificacion` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fecha_evento` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla kwsys.evento: ~4 rows (aproximadamente)
DELETE FROM `evento`;
/*!40000 ALTER TABLE `evento` DISABLE KEYS */;
INSERT INTO `evento` (`id`, `id_app`, `id_estado`, `hash_entrada`, `titulo`, `slug`, `imagen_principal`, `artistas`, `descripcion`, `fecha_creacion`, `fecha_modificacion`, `fecha_evento`) VALUES
	(47, 34, 9, '608f503697bc600dce466e22aa97aff5', 'Edicion #1', 'edicion--1', 'uploads/technoparty/entrada/evento/astronaut_8a9149956ca065aaf2e51a69a7651d20.gif', 'Una breve descripcion o anuncio de los dj232323', '', '2019-06-20 23:26:31', '2019-06-25 22:07:40', '2019-06-26'),
	(48, 34, 8, '608f503697bc600dce466e22aa97aff5', 'Edicion #2', 'edicion--1', 'uploads/technoparty/entrada/evento/astronaut_8a9149956ca065aaf2e51a69a7651d20.gif', 'Una breve descripcion o anuncio de los dj232323', '', '2019-06-20 23:26:31', '2019-06-25 22:38:26', '2019-06-26'),
	(49, 34, 7, '', 'mariano', 'mariano', 'uploads/technoparty/entrada/evento/astronaut_3cf21a11df061643fcc0bed896044405.gif', 'Una breve descripcion o anuncio de lo343434s dj', '', '2019-06-25 22:11:02', '2019-06-25 22:11:02', '2019-06-19');
/*!40000 ALTER TABLE `evento` ENABLE KEYS */;

-- Volcando estructura para tabla kwsys.objeto
CREATE TABLE IF NOT EXISTS `objeto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_app` int(11) NOT NULL,
  `slug` varchar(150) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla kwsys.objeto: ~4 rows (aproximadamente)
DELETE FROM `objeto`;
/*!40000 ALTER TABLE `objeto` DISABLE KEYS */;
INSERT INTO `objeto` (`id`, `id_app`, `slug`) VALUES
	(22, 34, 'evento'),
	(23, 34, 'entrada'),
	(24, 34, 'escaner'),
	(25, 34, 'perro'),
	(26, 34, 'perronsky');
/*!40000 ALTER TABLE `objeto` ENABLE KEYS */;

-- Volcando estructura para tabla kwsys.usuario
CREATE TABLE IF NOT EXISTS `usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_app` int(11) DEFAULT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `email` varchar(150) NOT NULL,
  `password` varchar(50) NOT NULL,
  `privilegio` enum('admin','regular','banned','suspended','pending') NOT NULL DEFAULT 'regular',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla kwsys.usuario: ~2 rows (aproximadamente)
DELETE FROM `usuario`;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` (`id`, `id_app`, `nombre`, `email`, `password`, `privilegio`) VALUES
	(1, 0, 'Kory', 'kory', '1234', 'admin'),
	(6, 34, 'federicolaztra', 'federicolaztra@gmail.com', 'ec6a6536ca304edf844d1d248a4f08dc', 'regular');
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
